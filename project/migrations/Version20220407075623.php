<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220407075623 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7D3656A4F85E0677 ON account (username)');
        $this->addSql('ALTER TABLE horse ADD object_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE horse ADD CONSTRAINT FK_629A2F18232D562B FOREIGN KEY (object_id) REFERENCES global_object (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_629A2F18232D562B ON horse (object_id)');
        $this->addSql('ALTER TABLE stable ADD object_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stable ADD CONSTRAINT FK_C7FA697232D562B FOREIGN KEY (object_id) REFERENCES global_object (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C7FA697232D562B ON stable (object_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_7D3656A4F85E0677 ON account');
        $this->addSql('ALTER TABLE horse DROP FOREIGN KEY FK_629A2F18232D562B');
        $this->addSql('DROP INDEX UNIQ_629A2F18232D562B ON horse');
        $this->addSql('ALTER TABLE horse DROP object_id');
        $this->addSql('ALTER TABLE stable DROP FOREIGN KEY FK_C7FA697232D562B');
        $this->addSql('DROP INDEX UNIQ_C7FA697232D562B ON stable');
        $this->addSql('ALTER TABLE stable DROP object_id');
    }
}
