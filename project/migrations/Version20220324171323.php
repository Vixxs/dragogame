<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220324171323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE banq_history (id INT AUTO_INCREMENT NOT NULL, account_id INT NOT NULL, transaction VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_30C0D00A9B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE global_object (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newspaper_event (id INT AUTO_INCREMENT NOT NULL, id_newspaper_id INT NOT NULL, UNIQUE INDEX UNIQ_37BF40C0E6180088 (id_newspaper_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newspaper_news (id INT AUTO_INCREMENT NOT NULL, id_newspaper_id INT NOT NULL, UNIQUE INDEX UNIQ_BA82C69BE6180088 (id_newspaper_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE banq_history ADD CONSTRAINT FK_30C0D00A9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE newspaper_event ADD CONSTRAINT FK_37BF40C0E6180088 FOREIGN KEY (id_newspaper_id) REFERENCES newspaper (id)');
        $this->addSql('ALTER TABLE newspaper_news ADD CONSTRAINT FK_BA82C69BE6180088 FOREIGN KEY (id_newspaper_id) REFERENCES newspaper (id)');
        $this->addSql('DROP TABLE like_object');
        $this->addSql('ALTER TABLE circuit DROP FOREIGN KEY FK_1325F3A666F16C78');
        $this->addSql('DROP INDEX IDX_1325F3A666F16C78 ON circuit');
        $this->addSql('ALTER TABLE circuit DROP id_infra_id');
        $this->addSql('ALTER TABLE competition DROP FOREIGN KEY FK_B50A2CB1BF84A342');
        $this->addSql('DROP INDEX IDX_B50A2CB1BF84A342 ON competition');
        $this->addSql('ALTER TABLE competition DROP id_club_id, CHANGE date�_end date_end TIME NOT NULL');
        $this->addSql('ALTER TABLE event ADD newspaper_event_id INT NOT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA75C49B25F FOREIGN KEY (newspaper_event_id) REFERENCES newspaper_event (id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA75C49B25F ON event (newspaper_event_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA75C49B25F');
        $this->addSql('CREATE TABLE like_object (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE banq_history');
        $this->addSql('DROP TABLE global_object');
        $this->addSql('DROP TABLE newspaper_event');
        $this->addSql('DROP TABLE newspaper_news');
        $this->addSql('ALTER TABLE circuit ADD id_infra_id INT NOT NULL');
        $this->addSql('ALTER TABLE circuit ADD CONSTRAINT FK_1325F3A666F16C78 FOREIGN KEY (id_infra_id) REFERENCES infrastructure (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_1325F3A666F16C78 ON circuit (id_infra_id)');
        $this->addSql('ALTER TABLE competition ADD id_club_id INT NOT NULL, CHANGE date_end date�_end TIME NOT NULL');
        $this->addSql('ALTER TABLE competition ADD CONSTRAINT FK_B50A2CB1BF84A342 FOREIGN KEY (id_club_id) REFERENCES ridding_club (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_B50A2CB1BF84A342 ON competition (id_club_id)');
        $this->addSql('DROP INDEX IDX_3BAE0AA75C49B25F ON event');
        $this->addSql('ALTER TABLE event DROP newspaper_event_id');
    }
}
