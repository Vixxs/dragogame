<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220325163341 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE competition_item (id INT AUTO_INCREMENT NOT NULL, competition_id INT DEFAULT NULL, item_id INT DEFAULT NULL, INDEX IDX_1158BB0C7B39D312 (competition_id), UNIQUE INDEX UNIQ_1158BB0C126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE competition_register (id INT AUTO_INCREMENT NOT NULL, competition_id INT DEFAULT NULL, account_id INT DEFAULT NULL, INDEX IDX_6E887B3B7B39D312 (competition_id), INDEX IDX_6E887B3B9B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horse_account (id INT AUTO_INCREMENT NOT NULL, horse_id INT DEFAULT NULL, account_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_C0DAC53A76B275AD (horse_id), INDEX IDX_C0DAC53A9B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horse_disease (id INT AUTO_INCREMENT NOT NULL, horse_id INT DEFAULT NULL, disease_id INT DEFAULT NULL, INDEX IDX_B2D7F95F76B275AD (horse_id), INDEX IDX_B2D7F95FD8355341 (disease_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horse_injury (id INT AUTO_INCREMENT NOT NULL, horse_id INT DEFAULT NULL, injury_id INT DEFAULT NULL, INDEX IDX_DB2BDBDD76B275AD (horse_id), INDEX IDX_DB2BDBDDABA45E9A (injury_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horse_item (id INT AUTO_INCREMENT NOT NULL, horse_id INT DEFAULT NULL, item_id INT DEFAULT NULL, INDEX IDX_4DB0022D76B275AD (horse_id), UNIQUE INDEX UNIQ_4DB0022D126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horse_parasite (id INT AUTO_INCREMENT NOT NULL, horse_id INT DEFAULT NULL, parasite_id INT DEFAULT NULL, INDEX IDX_59067FB776B275AD (horse_id), INDEX IDX_59067FB72450E1F (parasite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE infrastructure_item (id INT AUTO_INCREMENT NOT NULL, infrastructure_id INT DEFAULT NULL, item_id INT DEFAULT NULL, INDEX IDX_9949D2DB243E7A84 (infrastructure_id), UNIQUE INDEX UNIQ_9949D2DB126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item (id INT AUTO_INCREMENT NOT NULL, family_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, level INT NOT NULL, description LONGTEXT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, locked TINYINT(1) NOT NULL, INDEX IDX_1F1B251EC35E566A (family_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_family (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newspaper_advertising (id INT AUTO_INCREMENT NOT NULL, newspaper_id INT DEFAULT NULL, advertising_id INT DEFAULT NULL, INDEX IDX_B62C28ECC5D975FA (newspaper_id), INDEX IDX_B62C28EC9F084B42 (advertising_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newspaper_divers (id INT AUTO_INCREMENT NOT NULL, newspaper_id INT DEFAULT NULL, divers_id INT DEFAULT NULL, INDEX IDX_B112D11BC5D975FA (newspaper_id), INDEX IDX_B112D11B9C3BA491 (divers_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newspaper_weather (id INT AUTO_INCREMENT NOT NULL, newspaper_id INT DEFAULT NULL, weather_id INT DEFAULT NULL, INDEX IDX_F0658A91C5D975FA (newspaper_id), INDEX IDX_F0658A918CE675E (weather_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE planning (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, INDEX IDX_D499BFF69B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ridding_club_infra (id INT AUTO_INCREMENT NOT NULL, ridding_club_id INT DEFAULT NULL, infrastructure_id INT DEFAULT NULL, INDEX IDX_5C2FD5CA125ABB39 (ridding_club_id), UNIQUE INDEX UNIQ_5C2FD5CA243E7A84 (infrastructure_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ridding_club_member (id INT AUTO_INCREMENT NOT NULL, ridding_club_id INT DEFAULT NULL, account_id INT DEFAULT NULL, INDEX IDX_54EE6994125ABB39 (ridding_club_id), INDEX IDX_54EE69949B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stable (id INT AUTO_INCREMENT NOT NULL, account_id INT DEFAULT NULL, capacity INT NOT NULL, INDEX IDX_C7FA6979B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stable_infra (id INT AUTO_INCREMENT NOT NULL, stable_id INT DEFAULT NULL, infrastructure_id INT DEFAULT NULL, INDEX IDX_AD6576C6A6FA1C8B (stable_id), UNIQUE INDEX UNIQ_AD6576C6243E7A84 (infrastructure_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE task (id INT AUTO_INCREMENT NOT NULL, object_id INT DEFAULT NULL, planning_id INT DEFAULT NULL, action VARCHAR(255) NOT NULL, frequency INT NOT NULL, INDEX IDX_527EDB25232D562B (object_id), INDEX IDX_527EDB253D865311 (planning_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE competition_item ADD CONSTRAINT FK_1158BB0C7B39D312 FOREIGN KEY (competition_id) REFERENCES competition (id)');
        $this->addSql('ALTER TABLE competition_item ADD CONSTRAINT FK_1158BB0C126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE competition_register ADD CONSTRAINT FK_6E887B3B7B39D312 FOREIGN KEY (competition_id) REFERENCES competition (id)');
        $this->addSql('ALTER TABLE competition_register ADD CONSTRAINT FK_6E887B3B9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE horse_account ADD CONSTRAINT FK_C0DAC53A76B275AD FOREIGN KEY (horse_id) REFERENCES horse (id)');
        $this->addSql('ALTER TABLE horse_account ADD CONSTRAINT FK_C0DAC53A9B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE horse_disease ADD CONSTRAINT FK_B2D7F95F76B275AD FOREIGN KEY (horse_id) REFERENCES horse (id)');
        $this->addSql('ALTER TABLE horse_disease ADD CONSTRAINT FK_B2D7F95FD8355341 FOREIGN KEY (disease_id) REFERENCES disease (id)');
        $this->addSql('ALTER TABLE horse_injury ADD CONSTRAINT FK_DB2BDBDD76B275AD FOREIGN KEY (horse_id) REFERENCES horse (id)');
        $this->addSql('ALTER TABLE horse_injury ADD CONSTRAINT FK_DB2BDBDDABA45E9A FOREIGN KEY (injury_id) REFERENCES injury (id)');
        $this->addSql('ALTER TABLE horse_item ADD CONSTRAINT FK_4DB0022D76B275AD FOREIGN KEY (horse_id) REFERENCES horse (id)');
        $this->addSql('ALTER TABLE horse_item ADD CONSTRAINT FK_4DB0022D126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE horse_parasite ADD CONSTRAINT FK_59067FB776B275AD FOREIGN KEY (horse_id) REFERENCES horse (id)');
        $this->addSql('ALTER TABLE horse_parasite ADD CONSTRAINT FK_59067FB72450E1F FOREIGN KEY (parasite_id) REFERENCES parasite (id)');
        $this->addSql('ALTER TABLE infrastructure_item ADD CONSTRAINT FK_9949D2DB243E7A84 FOREIGN KEY (infrastructure_id) REFERENCES infrastructure (id)');
        $this->addSql('ALTER TABLE infrastructure_item ADD CONSTRAINT FK_9949D2DB126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251EC35E566A FOREIGN KEY (family_id) REFERENCES item_family (id)');
        $this->addSql('ALTER TABLE newspaper_advertising ADD CONSTRAINT FK_B62C28ECC5D975FA FOREIGN KEY (newspaper_id) REFERENCES newspaper (id)');
        $this->addSql('ALTER TABLE newspaper_advertising ADD CONSTRAINT FK_B62C28EC9F084B42 FOREIGN KEY (advertising_id) REFERENCES advertising (id)');
        $this->addSql('ALTER TABLE newspaper_divers ADD CONSTRAINT FK_B112D11BC5D975FA FOREIGN KEY (newspaper_id) REFERENCES newspaper (id)');
        $this->addSql('ALTER TABLE newspaper_divers ADD CONSTRAINT FK_B112D11B9C3BA491 FOREIGN KEY (divers_id) REFERENCES divers (id)');
        $this->addSql('ALTER TABLE newspaper_weather ADD CONSTRAINT FK_F0658A91C5D975FA FOREIGN KEY (newspaper_id) REFERENCES newspaper (id)');
        $this->addSql('ALTER TABLE newspaper_weather ADD CONSTRAINT FK_F0658A918CE675E FOREIGN KEY (weather_id) REFERENCES weather (id)');
        $this->addSql('ALTER TABLE planning ADD CONSTRAINT FK_D499BFF69B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE ridding_club_infra ADD CONSTRAINT FK_5C2FD5CA125ABB39 FOREIGN KEY (ridding_club_id) REFERENCES ridding_club (id)');
        $this->addSql('ALTER TABLE ridding_club_infra ADD CONSTRAINT FK_5C2FD5CA243E7A84 FOREIGN KEY (infrastructure_id) REFERENCES infrastructure (id)');
        $this->addSql('ALTER TABLE ridding_club_member ADD CONSTRAINT FK_54EE6994125ABB39 FOREIGN KEY (ridding_club_id) REFERENCES ridding_club (id)');
        $this->addSql('ALTER TABLE ridding_club_member ADD CONSTRAINT FK_54EE69949B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE stable ADD CONSTRAINT FK_C7FA6979B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE stable_infra ADD CONSTRAINT FK_AD6576C6A6FA1C8B FOREIGN KEY (stable_id) REFERENCES stable (id)');
        $this->addSql('ALTER TABLE stable_infra ADD CONSTRAINT FK_AD6576C6243E7A84 FOREIGN KEY (infrastructure_id) REFERENCES infrastructure (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB25232D562B FOREIGN KEY (object_id) REFERENCES global_object (id)');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB253D865311 FOREIGN KEY (planning_id) REFERENCES planning (id)');
        $this->addSql('ALTER TABLE circuit ADD id_infra_id INT NOT NULL');
        $this->addSql('ALTER TABLE circuit ADD CONSTRAINT FK_1325F3A666F16C78 FOREIGN KEY (id_infra_id) REFERENCES infrastructure (id)');
        $this->addSql('CREATE INDEX IDX_1325F3A666F16C78 ON circuit (id_infra_id)');
        $this->addSql('ALTER TABLE competition ADD id_club_id INT NOT NULL');
        $this->addSql('ALTER TABLE competition ADD CONSTRAINT FK_B50A2CB1BF84A342 FOREIGN KEY (id_club_id) REFERENCES ridding_club (id)');
        $this->addSql('CREATE INDEX IDX_B50A2CB1BF84A342 ON competition (id_club_id)');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA75C49B25F');
        $this->addSql('DROP INDEX IDX_3BAE0AA75C49B25F ON event');
        $this->addSql('ALTER TABLE event DROP newspaper_event_id');
        $this->addSql('ALTER TABLE infrastructure DROP family');
        $this->addSql('ALTER TABLE newspaper ADD key_point VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE newspaper_event DROP FOREIGN KEY FK_37BF40C0E6180088');
        $this->addSql('DROP INDEX UNIQ_37BF40C0E6180088 ON newspaper_event');
        $this->addSql('ALTER TABLE newspaper_event ADD newspaper_id INT DEFAULT NULL, ADD event_id INT DEFAULT NULL, DROP id_newspaper_id');
        $this->addSql('ALTER TABLE newspaper_event ADD CONSTRAINT FK_37BF40C0C5D975FA FOREIGN KEY (newspaper_id) REFERENCES newspaper (id)');
        $this->addSql('ALTER TABLE newspaper_event ADD CONSTRAINT FK_37BF40C071F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('CREATE INDEX IDX_37BF40C0C5D975FA ON newspaper_event (newspaper_id)');
        $this->addSql('CREATE INDEX IDX_37BF40C071F7E88B ON newspaper_event (event_id)');
        $this->addSql('ALTER TABLE newspaper_news DROP FOREIGN KEY FK_BA82C69BE6180088');
        $this->addSql('DROP INDEX UNIQ_BA82C69BE6180088 ON newspaper_news');
        $this->addSql('ALTER TABLE newspaper_news ADD newspaper_id INT DEFAULT NULL, ADD news_id INT DEFAULT NULL, DROP id_newspaper_id');
        $this->addSql('ALTER TABLE newspaper_news ADD CONSTRAINT FK_BA82C69BC5D975FA FOREIGN KEY (newspaper_id) REFERENCES newspaper (id)');
        $this->addSql('ALTER TABLE newspaper_news ADD CONSTRAINT FK_BA82C69BB5A459A0 FOREIGN KEY (news_id) REFERENCES news (id)');
        $this->addSql('CREATE INDEX IDX_BA82C69BC5D975FA ON newspaper_news (newspaper_id)');
        $this->addSql('CREATE INDEX IDX_BA82C69BB5A459A0 ON newspaper_news (news_id)');
        $this->addSql('ALTER TABLE ridding_club ADD account_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ridding_club ADD CONSTRAINT FK_638B09A59B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('CREATE INDEX IDX_638B09A59B6B5FBA ON ridding_club (account_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE competition_item DROP FOREIGN KEY FK_1158BB0C126F525E');
        $this->addSql('ALTER TABLE horse_item DROP FOREIGN KEY FK_4DB0022D126F525E');
        $this->addSql('ALTER TABLE infrastructure_item DROP FOREIGN KEY FK_9949D2DB126F525E');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251EC35E566A');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB253D865311');
        $this->addSql('ALTER TABLE stable_infra DROP FOREIGN KEY FK_AD6576C6A6FA1C8B');
        $this->addSql('DROP TABLE competition_item');
        $this->addSql('DROP TABLE competition_register');
        $this->addSql('DROP TABLE horse_account');
        $this->addSql('DROP TABLE horse_disease');
        $this->addSql('DROP TABLE horse_injury');
        $this->addSql('DROP TABLE horse_item');
        $this->addSql('DROP TABLE horse_parasite');
        $this->addSql('DROP TABLE infrastructure_item');
        $this->addSql('DROP TABLE item');
        $this->addSql('DROP TABLE item_family');
        $this->addSql('DROP TABLE newspaper_advertising');
        $this->addSql('DROP TABLE newspaper_divers');
        $this->addSql('DROP TABLE newspaper_weather');
        $this->addSql('DROP TABLE planning');
        $this->addSql('DROP TABLE ridding_club_infra');
        $this->addSql('DROP TABLE ridding_club_member');
        $this->addSql('DROP TABLE stable');
        $this->addSql('DROP TABLE stable_infra');
        $this->addSql('DROP TABLE task');
        $this->addSql('ALTER TABLE circuit DROP FOREIGN KEY FK_1325F3A666F16C78');
        $this->addSql('DROP INDEX IDX_1325F3A666F16C78 ON circuit');
        $this->addSql('ALTER TABLE circuit DROP id_infra_id');
        $this->addSql('ALTER TABLE competition DROP FOREIGN KEY FK_B50A2CB1BF84A342');
        $this->addSql('DROP INDEX IDX_B50A2CB1BF84A342 ON competition');
        $this->addSql('ALTER TABLE competition DROP id_club_id');
        $this->addSql('ALTER TABLE event ADD newspaper_event_id INT NOT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA75C49B25F FOREIGN KEY (newspaper_event_id) REFERENCES newspaper_event (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_3BAE0AA75C49B25F ON event (newspaper_event_id)');
        $this->addSql('ALTER TABLE infrastructure ADD family VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE newspaper DROP key_point');
        $this->addSql('ALTER TABLE newspaper_event DROP FOREIGN KEY FK_37BF40C0C5D975FA');
        $this->addSql('ALTER TABLE newspaper_event DROP FOREIGN KEY FK_37BF40C071F7E88B');
        $this->addSql('DROP INDEX IDX_37BF40C0C5D975FA ON newspaper_event');
        $this->addSql('DROP INDEX IDX_37BF40C071F7E88B ON newspaper_event');
        $this->addSql('ALTER TABLE newspaper_event ADD id_newspaper_id INT NOT NULL, DROP newspaper_id, DROP event_id');
        $this->addSql('ALTER TABLE newspaper_event ADD CONSTRAINT FK_37BF40C0E6180088 FOREIGN KEY (id_newspaper_id) REFERENCES newspaper (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_37BF40C0E6180088 ON newspaper_event (id_newspaper_id)');
        $this->addSql('ALTER TABLE newspaper_news DROP FOREIGN KEY FK_BA82C69BC5D975FA');
        $this->addSql('ALTER TABLE newspaper_news DROP FOREIGN KEY FK_BA82C69BB5A459A0');
        $this->addSql('DROP INDEX IDX_BA82C69BC5D975FA ON newspaper_news');
        $this->addSql('DROP INDEX IDX_BA82C69BB5A459A0 ON newspaper_news');
        $this->addSql('ALTER TABLE newspaper_news ADD id_newspaper_id INT NOT NULL, DROP newspaper_id, DROP news_id');
        $this->addSql('ALTER TABLE newspaper_news ADD CONSTRAINT FK_BA82C69BE6180088 FOREIGN KEY (id_newspaper_id) REFERENCES newspaper (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BA82C69BE6180088 ON newspaper_news (id_newspaper_id)');
        $this->addSql('ALTER TABLE ridding_club DROP FOREIGN KEY FK_638B09A59B6B5FBA');
        $this->addSql('DROP INDEX IDX_638B09A59B6B5FBA ON ridding_club');
        $this->addSql('ALTER TABLE ridding_club DROP account_id');
    }
}
