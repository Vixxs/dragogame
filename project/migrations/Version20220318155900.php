<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220318155900 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bank_history (id INT AUTO_INCREMENT NOT NULL, transaction DOUBLE PRECISION NOT NULL, date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE config DROP id_config');
        $this->addSql('ALTER TABLE disease DROP id_disease');
        $this->addSql('ALTER TABLE horse DROP id_horse');
        $this->addSql('ALTER TABLE injury DROP id_injury');
        $this->addSql('ALTER TABLE like_object DROP id_object');
        $this->addSql('ALTER TABLE newspaper DROP id_newspaper');
        $this->addSql('ALTER TABLE parasite DROP id_parasite');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE bank_history');
        $this->addSql('ALTER TABLE config ADD id_config INT NOT NULL');
        $this->addSql('ALTER TABLE disease ADD id_disease INT NOT NULL');
        $this->addSql('ALTER TABLE horse ADD id_horse INT NOT NULL');
        $this->addSql('ALTER TABLE injury ADD id_injury INT NOT NULL');
        $this->addSql('ALTER TABLE like_object ADD id_object INT NOT NULL');
        $this->addSql('ALTER TABLE newspaper ADD id_newspaper INT NOT NULL');
        $this->addSql('ALTER TABLE parasite ADD id_parasite INT NOT NULL');
    }
}
