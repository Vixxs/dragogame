<?php

namespace App\Controller\Admin;

use App\Entity\NewspaperNews;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class NewspaperNewsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NewspaperNews::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('newspaper'),
                AssociationField::new('news')
            ]
        );
    }
}
