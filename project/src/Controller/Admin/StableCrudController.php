<?php

namespace App\Controller\Admin;

use App\Entity\Stable;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class StableCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Stable::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('account')
            ]
        );
    }
}
