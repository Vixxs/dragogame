<?php

namespace App\Controller\Admin;

use App\Entity\NewspaperEvent;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class NewspaperEventCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NewspaperEvent::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('newspaper'),
                AssociationField::new('event')
            ]
        );
    }
}
