<?php

namespace App\Controller\Admin;

use App\Entity\HorseInjury;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class HorseInjuryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HorseInjury::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('horse'),
                AssociationField::new('injury')
            ]
        );
    }
}
