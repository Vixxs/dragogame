<?php

namespace App\Controller\Admin;

use App\Entity\NewspaperWeather;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class NewspaperWeatherCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NewspaperWeather::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('newspaper'),
                AssociationField::new('weather')
            ]
        );
    }
}
