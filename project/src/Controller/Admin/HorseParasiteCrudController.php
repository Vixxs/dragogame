<?php

namespace App\Controller\Admin;

use App\Entity\HorseParasite;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class HorseParasiteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HorseParasite::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('horse'),
                AssociationField::new('parasite')
            ]
        );
    }
}
