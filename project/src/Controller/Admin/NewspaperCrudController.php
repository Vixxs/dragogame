<?php

namespace App\Controller\Admin;

use App\Entity\Newspaper;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class NewspaperCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Newspaper::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
