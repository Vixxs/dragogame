<?php

namespace App\Controller\Admin;

use App\Entity\ItemFamily;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ItemFamilyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ItemFamily::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
