<?php

namespace App\Controller\Admin;

use App\Entity\StableInfra;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class StableInfraCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StableInfra::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('stable'),
                AssociationField::new('infrastructure')
            ]
        );
    }
}
