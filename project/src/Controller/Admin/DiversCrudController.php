<?php

namespace App\Controller\Admin;

use App\Entity\Divers;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DiversCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Divers::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
