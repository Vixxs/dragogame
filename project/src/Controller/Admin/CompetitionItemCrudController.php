<?php

namespace App\Controller\Admin;

use App\Entity\CompetitionItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class CompetitionItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CompetitionItem::class;
    }
    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('competition'),
                AssociationField::new('item')
            ]
        );
    }
}
