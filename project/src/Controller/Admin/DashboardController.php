<?php

namespace App\Controller\Admin;

use App\Entity\Account;
use App\Entity\BanqHistory;
use App\Entity\Circuit;
use App\Entity\Competition;
use App\Entity\CompetitionItem;
use App\Entity\CompetitionRegister;
use App\Entity\Config;
use App\Entity\Disease;
use App\Entity\Divers;
use App\Entity\Event;
use App\Entity\Horse;
use App\Entity\HorseAccount;
use App\Entity\HorseDisease;
use App\Entity\HorseInjury;
use App\Entity\HorseItem;
use App\Entity\HorseParasite;
use App\Entity\Infrastructure;
use App\Entity\InfrastructureItem;
use App\Entity\Injury;
use App\Entity\Item;
use App\Entity\ItemFamily;
use App\Entity\News;
use App\Entity\Newspaper;
use App\Entity\NewspaperDivers;
use App\Entity\NewspaperEvent;
use App\Entity\NewspaperNews;
use App\Entity\NewspaperAdvertising;
use App\Entity\NewspaperWeather;
use App\Entity\Parasite;
use App\Entity\Planning;
use App\Entity\RiddingClub;
use App\Entity\RiddingClubInfra;
use App\Entity\RiddingClubMember;
use App\Entity\Stable;
use App\Entity\StableInfra;
use App\Entity\Task;
use App\Entity\Weather;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\MenuFactory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;
class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {

        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        // Option 1. Make your dashboard redirect to the same page for all users
        return $this->redirect($adminUrlGenerator->setController(AccountCrudController::class)->generateUrl());

    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle("Dragogame")
            ->setFaviconPath('images/icon.png');
    }

    public function configureMenuItems(): iterable
    {
        //yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Account', 'fas fa-user', Account::class);
        yield MenuItem::linkToCrud('Config', 'fas fa-wrench', Config::class);
        yield MenuItem::linkToCrud('Bank History', 'fas fa-wallet', BanqHistory::class);
        yield MenuItem::linkToCrud('Circuit', 'fas fa-flag-checkered', Circuit::class);
        yield MenuItem::linkToCrud('Competition', 'fas fa-award', Competition::class);
        yield MenuItem::linkToCrud('Competition Items', 'fas fa-award', CompetitionItem::class);
        yield MenuItem::linkToCrud('Competition Register', 'fas fa-award', CompetitionRegister::class);
        yield MenuItem::linkToCrud('Horse', 'fas fa-horse-head', Horse::class);
        yield MenuItem::linkToCrud('Horse Account', 'fas fa-horse-head', HorseAccount::class);
        yield MenuItem::linkToCrud('Horse Disease', 'fas fa-horse-head', HorseDisease::class);
        yield MenuItem::linkToCrud('Horse Injury', 'fas fa-horse-head', HorseInjury::class);
        yield MenuItem::linkToCrud('Horse Item', 'fas fa-horse-head', HorseItem::class);
        yield MenuItem::linkToCrud('Horse Parasite', 'fas fa-horse-head', HorseParasite::class);
        yield MenuItem::linkToCrud('Infrastructure', 'fas fa-hammer', Infrastructure::class);
        yield MenuItem::linkToCrud('Infrastructure Item', 'fas fa-hammer', InfrastructureItem::class);
        yield MenuItem::linkToCrud('Item', 'fas fa-box-open', Item::class);
        yield MenuItem::linkToCrud('Item Family', 'fas fa-box-open', ItemFamily::class);
        yield MenuItem::linkToCrud('Ridding Club', 'fas fa-hat-cowboy', RiddingClub::class);
        yield MenuItem::linkToCrud('Ridding Club Infra', 'fas fa-hat-cowboy', RiddingClubInfra::class);
        yield MenuItem::linkToCrud('Ridding Club Member', 'fas fa-hat-cowboy', RiddingClubMember::class);
        yield MenuItem::linkToCrud('Stable', 'fas fa-warehouse', Stable::class);
        yield MenuItem::linkToCrud('Stable Infra', 'fas fa-warehouse', StableInfra::class);
        yield MenuItem::linkToCrud('Planning', 'fas fa-calendar-week', Planning::class);
        yield MenuItem::linkToCrud('Task', 'fas fa-calendar', Task::class);
        yield MenuItem::linkToCrud('Event', 'fas fa-calendar-check', Event::class);
        yield MenuItem::linkToCrud('Weather', 'fas fa-cloud', Weather::class);
        yield MenuItem::linkToCrud('Disease', 'fas fa-virus', Disease::class);
        yield MenuItem::linkToCrud('Parasite', 'fas fa-bug', Parasite::class);
        yield MenuItem::linkToCrud('Injury', 'fas fa-crutch', Injury::class);
        yield MenuItem::linkToCrud('Divers', 'fas fa-clone', Divers::class);
        yield MenuItem::linkToCrud('News', 'fas fa-newspaper', News::class);
        yield MenuItem::linkToCrud('Newspaper', 'fas fa-newspaper', Newspaper::class);
        yield MenuItem::linkToCrud('Newspaper Advertising', 'fas fa-newspaper', NewspaperAdvertising::class);
        yield MenuItem::linkToCrud('Newspaper Divers', 'fas fa-newspaper', NewspaperDivers::class);
        yield MenuItem::linkToCrud('Newspaper Event', 'fas fa-newspaper', NewspaperEvent::class);
        yield MenuItem::linkToCrud('Newspaper News', 'fas fa-newspaper', NewspaperNews::class);
        yield MenuItem::linkToCrud('Newspaper Weather', 'fas fa-newspaper', NewspaperWeather::class);
    }
}
