<?php

namespace App\Controller\Admin;

use App\Entity\NewspaperAdvertising;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class NewspaperAdvertisingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NewspaperAdvertising::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('newspaper'),
                AssociationField::new('advertising')
            ]
        );
    }
}
