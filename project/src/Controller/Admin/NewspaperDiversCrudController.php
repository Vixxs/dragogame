<?php

namespace App\Controller\Admin;

use App\Entity\NewspaperDivers;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class NewspaperDiversCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return NewspaperDivers::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('newspaper'),
                AssociationField::new('divers')
            ]
        );
    }
}
