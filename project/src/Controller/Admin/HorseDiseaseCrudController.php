<?php

namespace App\Controller\Admin;

use App\Entity\HorseDisease;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class HorseDiseaseCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HorseDisease::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('horse'),
                AssociationField::new('disease')
            ]
        );
    }
}
