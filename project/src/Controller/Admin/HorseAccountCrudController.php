<?php

namespace App\Controller\Admin;

use App\Entity\HorseAccount;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class HorseAccountCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HorseAccount::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('horse'),
                AssociationField::new('account')
            ]
        );
    }
}
