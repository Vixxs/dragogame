<?php

namespace App\Controller\Admin;

use App\Entity\HorseItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class HorseItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HorseItem::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('horse'),
                AssociationField::new('item')
            ]
        );
    }
}
