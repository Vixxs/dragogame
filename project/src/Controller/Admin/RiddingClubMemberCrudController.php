<?php

namespace App\Controller\Admin;

use App\Entity\RiddingClubMember;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class RiddingClubMemberCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RiddingClubMember::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('riddingClub'),
                AssociationField::new('account')
            ]
        );
    }
}
