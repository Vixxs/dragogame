<?php

namespace App\Controller\Admin;

use App\Entity\RiddingClub;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class RiddingClubCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RiddingClub::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return array_merge(parent::configureFields($pageName), 
            [
                AssociationField::new('account')
            ]
        );
    }
}
