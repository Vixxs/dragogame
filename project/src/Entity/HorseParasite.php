<?php

namespace App\Entity;

use App\Repository\HorseParasiteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HorseParasiteRepository::class)
 */
class HorseParasite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Horse::class, inversedBy="horseParasites")
     */
    private $horse;

    /**
     * @ORM\ManyToOne(targetEntity=Parasite::class, inversedBy="horseParasites")
     */
    private $parasite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHorse(): ?Horse
    {
        return $this->horse;
    }

    public function setHorse(?Horse $horse): self
    {
        $this->horse = $horse;

        return $this;
    }

    public function getParasite(): ?Parasite
    {
        return $this->parasite;
    }

    public function setParasite(?Parasite $parasite): self
    {
        $this->parasite = $parasite;

        return $this;
    }
}
