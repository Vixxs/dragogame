<?php

namespace App\Entity;

use App\Repository\DiversRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DiversRepository::class)
 */
class Divers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperDivers::class, mappedBy="divers")
     */
    private $newspaperDivers;

    public function __construct()
    {
        $this->newspaperDivers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, NewspaperDivers>
     */
    public function getNewspaperDivers(): Collection
    {
        return $this->newspaperDivers;
    }

    public function addNewspaperDiver(NewspaperDivers $newspaperDiver): self
    {
        if (!$this->newspaperDivers->contains($newspaperDiver)) {
            $this->newspaperDivers[] = $newspaperDiver;
            $newspaperDiver->setDivers($this);
        }

        return $this;
    }

    public function removeNewspaperDiver(NewspaperDivers $newspaperDiver): self
    {
        if ($this->newspaperDivers->removeElement($newspaperDiver)) {
            // set the owning side to null (unless already changed)
            if ($newspaperDiver->getDivers() === $this) {
                $newspaperDiver->setDivers(null);
            }
        }

        return $this;
    }
}
