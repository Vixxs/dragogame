<?php

namespace App\Entity;

use App\Repository\NewspaperEventRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewspaperEventRepository::class)
 */
class NewspaperEvent
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Newspaper::class, inversedBy="newspaperEvents")
     */
    private $newspaper;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="newspaperEvents")
     */
    private $event;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNewspaper(): ?Newspaper
    {
        return $this->newspaper;
    }

    public function setNewspaper(?Newspaper $newspaper): self
    {
        $this->newspaper = $newspaper;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }
}
