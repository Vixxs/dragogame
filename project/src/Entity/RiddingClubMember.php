<?php

namespace App\Entity;

use App\Repository\RiddingClubMemberRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RiddingClubMemberRepository::class)
 */
class RiddingClubMember
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=RiddingClub::class, inversedBy="members")
     */
    private $riddingClub;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="riddingClubMembers")
     */
    private $account;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRiddingClub(): ?RiddingClub
    {
        return $this->riddingClub;
    }

    public function setRiddingClub(?RiddingClub $riddingClub): self
    {
        $this->riddingClub = $riddingClub;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }
}
