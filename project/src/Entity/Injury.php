<?php

namespace App\Entity;

use App\Repository\InjuryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InjuryRepository::class)
 */
class Injury
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=HorseInjury::class, mappedBy="injury")
     */
    private $horseInjuries;

    public function __construct()
    {
        $this->horseInjuries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, HorseInjury>
     */
    public function getHorseInjuries(): Collection
    {
        return $this->horseInjuries;
    }

    public function addHorseInjury(HorseInjury $horseInjury): self
    {
        if (!$this->horseInjuries->contains($horseInjury)) {
            $this->horseInjuries[] = $horseInjury;
            $horseInjury->setInjury($this);
        }

        return $this;
    }

    public function removeHorseInjury(HorseInjury $horseInjury): self
    {
        if ($this->horseInjuries->removeElement($horseInjury)) {
            // set the owning side to null (unless already changed)
            if ($horseInjury->getInjury() === $this) {
                $horseInjury->setInjury(null);
            }
        }

        return $this;
    }
}
