<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperEvent::class, mappedBy="event")
     */
    private $newspaperEvents;

    public function __construct()
    {
        $this->newspaperEvents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, NewspaperEvent>
     */
    public function getNewspaperEvents(): Collection
    {
        return $this->newspaperEvents;
    }

    public function addNewspaperEvent(NewspaperEvent $newspaperEvent): self
    {
        if (!$this->newspaperEvents->contains($newspaperEvent)) {
            $this->newspaperEvents[] = $newspaperEvent;
            $newspaperEvent->setEvent($this);
        }

        return $this;
    }

    public function removeNewspaperEvent(NewspaperEvent $newspaperEvent): self
    {
        if ($this->newspaperEvents->removeElement($newspaperEvent)) {
            // set the owning side to null (unless already changed)
            if ($newspaperEvent->getEvent() === $this) {
                $newspaperEvent->setEvent(null);
            }
        }

        return $this;
    }
}
