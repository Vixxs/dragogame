<?php

namespace App\Entity;

use App\Repository\DiseaseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DiseaseRepository::class)
 */
class Disease
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=HorseDisease::class, mappedBy="disease")
     */
    private $horseDiseases;

    public function __construct()
    {
        $this->horseDiseases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, HorseDisease>
     */
    public function getHorseDiseases(): Collection
    {
        return $this->horseDiseases;
    }

    public function addHorseDisease(HorseDisease $horseDisease): self
    {
        if (!$this->horseDiseases->contains($horseDisease)) {
            $this->horseDiseases[] = $horseDisease;
            $horseDisease->setDisease($this);
        }

        return $this;
    }

    public function removeHorseDisease(HorseDisease $horseDisease): self
    {
        if ($this->horseDiseases->removeElement($horseDisease)) {
            // set the owning side to null (unless already changed)
            if ($horseDisease->getDisease() === $this) {
                $horseDisease->setDisease(null);
            }
        }

        return $this;
    }
}
