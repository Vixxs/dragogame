<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use App\Repository\AccountRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=AccountRepository::class)
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class Account implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="float")
     */
    private $money = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="date")
     */
    private $birthdate;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sexe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_number;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_sign_in;  

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_last_connection;

    /**
     * @ORM\OneToMany(targetEntity=BanqHistory::class, mappedBy="account")
     */
    private $banqHistories;

    /**
     * @ORM\OneToMany(targetEntity=Stable::class, mappedBy="account")
     */
    private $stables;

    /**
     * @ORM\OneToMany(targetEntity=RiddingClub::class, mappedBy="account")
     */
    private $riddingClubs;

    /**
     * @ORM\OneToMany(targetEntity=RiddingClubMember::class, mappedBy="account")
     */
    private $riddingClubMembers;

    /**
     * @ORM\OneToMany(targetEntity=Planning::class, mappedBy="account")
     */
    private $plannings;

    /**
     * @ORM\OneToMany(targetEntity=HorseAccount::class, mappedBy="account")
     */
    private $horseAccounts;

    /**
     * @ORM\OneToMany(targetEntity=CompetitionRegister::class, mappedBy="account")
     */
    private $competitionRegisters;

    public function __construct()
    {
        $this->banqHistories = new ArrayCollection();
        $this->stables = new ArrayCollection();
        $this->riddingClubs = new ArrayCollection();
        $this->riddingClubMembers = new ArrayCollection();
        $this->plannings = new ArrayCollection();
        $this->horseAccounts = new ArrayCollection();
        $this->competitionRegisters = new ArrayCollection();
        $this->ip = key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR']: '';
        $this->date_sign_in = new \DateTime();
        $this->date_last_connection = new \DateTime();
    }

    public function __toString()
    {
        return $this->getEmail();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getMoney(): ?float
    {
        return $this->money;
    }

    public function setMoney(float $money): self
    {
        $this->money = $money;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDateSignIn(): ?\DateTimeInterface
    {
        return $this->date_sign_in;
    }

    public function setDateSignIn(\DateTimeInterface $date_sign_in): self
    {
        $this->date_sign_in = $date_sign_in;

        return $this;
    }

    public function getDateLastConnection(): ?\DateTimeInterface
    {
        return $this->date_last_connection;
    }

    public function setDateLastConnection(\DateTimeInterface $date_last_connection): self
    {
        $this->date_last_connection = $date_last_connection;

        return $this;
    }

    /**
     * @return Collection<int, BanqHistory>
     */
    public function getBanqHistories(): Collection
    {
        return $this->banqHistories;
    }

    public function addBanqHistory(BanqHistory $banqHistory): self
    {
        if (!$this->banqHistories->contains($banqHistory)) {
            $this->banqHistories[] = $banqHistory;
            $banqHistory->setRelation($this);
        }

        return $this;
    }

    public function removeBanqHistory(BanqHistory $banqHistory): self
    {
        if ($this->banqHistories->removeElement($banqHistory)) {
            // set the owning side to null (unless already changed)
            if ($banqHistory->getRelation() === $this) {
                $banqHistory->setRelation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Stable>
     */
    public function getStables(): Collection
    {
        return $this->stables;
    }

    public function addStable(Stable $stable): self
    {
        if (!$this->stables->contains($stable)) {
            $this->stables[] = $stable;
            $stable->setAccount($this);
        }

        return $this;
    }

    public function removeStable(Stable $stable): self
    {
        if ($this->stables->removeElement($stable)) {
            // set the owning side to null (unless already changed)
            if ($stable->getAccount() === $this) {
                $stable->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RiddingClub>
     */
    public function getRiddingClubs(): Collection
    {
        return $this->riddingClubs;
    }

    public function addRiddingClub(RiddingClub $riddingClub): self
    {
        if (!$this->riddingClubs->contains($riddingClub)) {
            $this->riddingClubs[] = $riddingClub;
            $riddingClub->setAccount($this);
        }

        return $this;
    }

    public function removeRiddingClub(RiddingClub $riddingClub): self
    {
        if ($this->riddingClubs->removeElement($riddingClub)) {
            // set the owning side to null (unless already changed)
            if ($riddingClub->getAccount() === $this) {
                $riddingClub->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RiddingClubMember>
     */
    public function getRiddingClubMembers(): Collection
    {
        return $this->riddingClubMembers;
    }

    public function addRiddingClubMember(RiddingClubMember $riddingClubMember): self
    {
        if (!$this->riddingClubMembers->contains($riddingClubMember)) {
            $this->riddingClubMembers[] = $riddingClubMember;
            $riddingClubMember->setAccount($this);
        }

        return $this;
    }

    public function removeRiddingClubMember(RiddingClubMember $riddingClubMember): self
    {
        if ($this->riddingClubMembers->removeElement($riddingClubMember)) {
            // set the owning side to null (unless already changed)
            if ($riddingClubMember->getAccount() === $this) {
                $riddingClubMember->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Planning>
     */
    public function getPlannings(): Collection
    {
        return $this->plannings;
    }

    public function addPlanning(Planning $planning): self
    {
        if (!$this->plannings->contains($planning)) {
            $this->plannings[] = $planning;
            $planning->setAccount($this);
        }

        return $this;
    }

    public function removePlanning(Planning $planning): self
    {
        if ($this->plannings->removeElement($planning)) {
            // set the owning side to null (unless already changed)
            if ($planning->getAccount() === $this) {
                $planning->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, HorseAccount>
     */
    public function getHorseAccounts(): Collection
    {
        return $this->horseAccounts;
    }

    public function addHorseAccount(HorseAccount $horseAccount): self
    {
        if (!$this->horseAccounts->contains($horseAccount)) {
            $this->horseAccounts[] = $horseAccount;
            $horseAccount->setAccount($this);
        }

        return $this;
    }

    public function removeHorseAccount(HorseAccount $horseAccount): self
    {
        if ($this->horseAccounts->removeElement($horseAccount)) {
            // set the owning side to null (unless already changed)
            if ($horseAccount->getAccount() === $this) {
                $horseAccount->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CompetitionRegister>
     */
    public function getCompetitionRegisters(): Collection
    {
        return $this->competitionRegisters;
    }

    public function addCompetitionRegister(CompetitionRegister $competitionRegister): self
    {
        if (!$this->competitionRegisters->contains($competitionRegister)) {
            $this->competitionRegisters[] = $competitionRegister;
            $competitionRegister->setAccount($this);
        }

        return $this;
    }

    public function removeCompetitionRegister(CompetitionRegister $competitionRegister): self
    {
        if ($this->competitionRegisters->removeElement($competitionRegister)) {
            // set the owning side to null (unless already changed)
            if ($competitionRegister->getAccount() === $this) {
                $competitionRegister->setAccount(null);
            }
        }

        return $this;
    }

}
