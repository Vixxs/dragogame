<?php

namespace App\Entity;

use App\Repository\WeatherRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WeatherRepository::class)
 */
class Weather
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperWeather::class, mappedBy="weather")
     */
    private $newspaperWeather;

    public function __construct()
    {
        $this->newspaperWeather = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, NewspaperWeather>
     */
    public function getNewspaperWeather(): Collection
    {
        return $this->newspaperWeather;
    }

    public function addNewspaperWeather(NewspaperWeather $newspaperWeather): self
    {
        if (!$this->newspaperWeather->contains($newspaperWeather)) {
            $this->newspaperWeather[] = $newspaperWeather;
            $newspaperWeather->setWeather($this);
        }

        return $this;
    }

    public function removeNewspaperWeather(NewspaperWeather $newspaperWeather): self
    {
        if ($this->newspaperWeather->removeElement($newspaperWeather)) {
            // set the owning side to null (unless already changed)
            if ($newspaperWeather->getWeather() === $this) {
                $newspaperWeather->setWeather(null);
            }
        }

        return $this;
    }
}
