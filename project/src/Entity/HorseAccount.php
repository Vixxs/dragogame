<?php

namespace App\Entity;

use App\Repository\HorseAccountRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HorseAccountRepository::class)
 */
class HorseAccount
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Horse::class, cascade={"persist", "remove"})
     */
    private $horse;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="horseAccounts")
     */
    private $account;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHorse(): ?Horse
    {
        return $this->horse;
    }

    public function setHorse(?Horse $horse): self
    {
        $this->horse = $horse;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }
}
