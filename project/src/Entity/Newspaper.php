<?php

namespace App\Entity;

use App\Repository\NewspaperRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewspaperRepository::class)
 */
class Newspaper
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $key_point;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperNews::class, mappedBy="newspaper")
     */
    private $newspaperNews;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperEvent::class, mappedBy="newspaper")
     */
    private $newspaperEvents;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperAdvertising::class, mappedBy="newspaper")
     */
    private $newspaperAdvertisings;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperWeather::class, mappedBy="newspaper")
     */
    private $newspaperWeather;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperDivers::class, mappedBy="newspaper")
     */
    private $newspaperDivers;

    public function __construct()
    {
        $this->newspaperNews = new ArrayCollection();
        $this->newspaperEvents = new ArrayCollection();
        $this->newspaperAdvertisings = new ArrayCollection();
        $this->newspaperWeather = new ArrayCollection();
        $this->newspaperDivers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getKeyPoint(): ?string
    {
        return $this->key_point;
    }

    public function setKeyPoint(string $key_point): self
    {
        $this->key_point = $key_point;

        return $this;
    }

    /**
     * @return Collection<int, NewspaperNews>
     */
    public function getNewspaperNews(): Collection
    {
        return $this->newspaperNews;
    }

    public function addNewspaperNews(NewspaperNews $newspaperNews): self
    {
        if (!$this->newspaperNews->contains($newspaperNews)) {
            $this->newspaperNews[] = $newspaperNews;
            $newspaperNews->setNewspaper($this);
        }

        return $this;
    }

    public function removeNewspaperNews(NewspaperNews $newspaperNews): self
    {
        if ($this->newspaperNews->removeElement($newspaperNews)) {
            // set the owning side to null (unless already changed)
            if ($newspaperNews->getNewspaper() === $this) {
                $newspaperNews->setNewspaper(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NewspaperEvent>
     */
    public function getNewspaperEvents(): Collection
    {
        return $this->newspaperEvents;
    }

    public function addNewspaperEvent(NewspaperEvent $newspaperEvent): self
    {
        if (!$this->newspaperEvents->contains($newspaperEvent)) {
            $this->newspaperEvents[] = $newspaperEvent;
            $newspaperEvent->setNewspaper($this);
        }

        return $this;
    }

    public function removeNewspaperEvent(NewspaperEvent $newspaperEvent): self
    {
        if ($this->newspaperEvents->removeElement($newspaperEvent)) {
            // set the owning side to null (unless already changed)
            if ($newspaperEvent->getNewspaper() === $this) {
                $newspaperEvent->setNewspaper(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NewspaperAdvertising>
     */
    public function getNewspaperAdvertisings(): Collection
    {
        return $this->newspaperAdvertisings;
    }

    public function addNewspaperAdvertising(NewspaperAdvertising $newspaperAdvertising): self
    {
        if (!$this->newspaperAdvertisings->contains($newspaperAdvertising)) {
            $this->newspaperAdvertisings[] = $newspaperAdvertising;
            $newspaperAdvertising->setNewspaper($this);
        }

        return $this;
    }

    public function removeNewspaperAdvertising(NewspaperAdvertising $newspaperAdvertising): self
    {
        if ($this->newspaperAdvertisings->removeElement($newspaperAdvertising)) {
            // set the owning side to null (unless already changed)
            if ($newspaperAdvertising->getNewspaper() === $this) {
                $newspaperAdvertising->setNewspaper(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NewspaperWeather>
     */
    public function getNewspaperWeather(): Collection
    {
        return $this->newspaperWeather;
    }

    public function addNewspaperWeather(NewspaperWeather $newspaperWeather): self
    {
        if (!$this->newspaperWeather->contains($newspaperWeather)) {
            $this->newspaperWeather[] = $newspaperWeather;
            $newspaperWeather->setNewspaper($this);
        }

        return $this;
    }

    public function removeNewspaperWeather(NewspaperWeather $newspaperWeather): self
    {
        if ($this->newspaperWeather->removeElement($newspaperWeather)) {
            // set the owning side to null (unless already changed)
            if ($newspaperWeather->getNewspaper() === $this) {
                $newspaperWeather->setNewspaper(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, NewspaperDivers>
     */
    public function getNewspaperDivers(): Collection
    {
        return $this->newspaperDivers;
    }

    public function addNewspaperDiver(NewspaperDivers $newspaperDiver): self
    {
        if (!$this->newspaperDivers->contains($newspaperDiver)) {
            $this->newspaperDivers[] = $newspaperDiver;
            $newspaperDiver->setNewspaper($this);
        }

        return $this;
    }

    public function removeNewspaperDiver(NewspaperDivers $newspaperDiver): self
    {
        if ($this->newspaperDivers->removeElement($newspaperDiver)) {
            // set the owning side to null (unless already changed)
            if ($newspaperDiver->getNewspaper() === $this) {
                $newspaperDiver->setNewspaper(null);
            }
        }

        return $this;
    }
}
