<?php

namespace App\Entity;

use App\Repository\ParasiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParasiteRepository::class)
 */
class Parasite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=HorseParasite::class, mappedBy="parasite")
     */
    private $horseParasites;

    public function __construct()
    {
        $this->horseParasites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, HorseParasite>
     */
    public function getHorseParasites(): Collection
    {
        return $this->horseParasites;
    }

    public function addHorseParasite(HorseParasite $horseParasite): self
    {
        if (!$this->horseParasites->contains($horseParasite)) {
            $this->horseParasites[] = $horseParasite;
            $horseParasite->setParasite($this);
        }

        return $this;
    }

    public function removeHorseParasite(HorseParasite $horseParasite): self
    {
        if ($this->horseParasites->removeElement($horseParasite)) {
            // set the owning side to null (unless already changed)
            if ($horseParasite->getParasite() === $this) {
                $horseParasite->setParasite(null);
            }
        }

        return $this;
    }
}
