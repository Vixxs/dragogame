<?php

namespace App\Entity;

use App\Repository\AdvertisingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdvertisingRepository::class)
 */
class Advertising
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=NewspaperAdvertising::class, mappedBy="advertising")
     */
    private $newspaperAdvertisings;

    public function __construct()
    {
        $this->newspaperAdvertisings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, NewspaperAdvertising>
     */
    public function getNewspaperAdvertisings(): Collection
    {
        return $this->newspaperAdvertisings;
    }

    public function addNewspaperAdvertising(NewspaperAdvertising $newspaperAdvertising): self
    {
        if (!$this->newspaperAdvertisings->contains($newspaperAdvertising)) {
            $this->newspaperAdvertisings[] = $newspaperAdvertising;
            $newspaperAdvertising->setAdvertising($this);
        }

        return $this;
    }

    public function removeNewspaperAdvertising(NewspaperAdvertising $newspaperAdvertising): self
    {
        if ($this->newspaperAdvertisings->removeElement($newspaperAdvertising)) {
            // set the owning side to null (unless already changed)
            if ($newspaperAdvertising->getAdvertising() === $this) {
                $newspaperAdvertising->setAdvertising(null);
            }
        }

        return $this;
    }
}
