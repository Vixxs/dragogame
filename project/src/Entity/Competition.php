<?php

namespace App\Entity;

use App\Repository\CompetitionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompetitionRepository::class)
 */class Competition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=RiddingClub::class, inversedBy="competitions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_club;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;
    /**
     * @ORM\Column(type="datetime")
     */
    private $date_end;

    /**
     * @ORM\Column(type="float")
     */
    private $subscriber_price;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity=CompetitionItem::class, mappedBy="competition")
     */
    private $competitionItems;

    /**
     * @ORM\OneToMany(targetEntity=CompetitionRegister::class, mappedBy="competition")
     */
    private $competitionRegisters;

    public function __construct()
    {
        $this->competitionItems = new ArrayCollection();
        $this->competitionRegisters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdClub(): ?RiddingClub
    {
        return $this->id_club;
    }

    public function setIdClub(?RiddingClub $id_club): self
    {
        $this->id_club = $id_club;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->date_end;
    }

    public function setDateEnd(\DateTimeInterface $date_end): self
    {
        $this->date_end = $date_end;

        return $this;
    }

    public function getSubscriberPrice(): ?float
    {
        return $this->subscriber_price;
    }

    public function setSubscriberPrice(float $subscriber_price): self
    {
        $this->subscriber_price = $subscriber_price;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection<int, CompetitionItem>
     */
    public function getCompetitionItems(): Collection
    {
        return $this->competitionItems;
    }

    public function addCompetitionItem(CompetitionItem $competitionItem): self
    {
        if (!$this->competitionItems->contains($competitionItem)) {
            $this->competitionItems[] = $competitionItem;
            $competitionItem->setCompetition($this);
        }

        return $this;
    }

    public function removeCompetitionItem(CompetitionItem $competitionItem): self
    {
        if ($this->competitionItems->removeElement($competitionItem)) {
            // set the owning side to null (unless already changed)
            if ($competitionItem->getCompetition() === $this) {
                $competitionItem->setCompetition(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CompetitionRegister>
     */
    public function getCompetitionRegisters(): Collection
    {
        return $this->competitionRegisters;
    }

    public function addCompetitionRegister(CompetitionRegister $competitionRegister): self
    {
        if (!$this->competitionRegisters->contains($competitionRegister)) {
            $this->competitionRegisters[] = $competitionRegister;
            $competitionRegister->setCompetition($this);
        }

        return $this;
    }

    public function removeCompetitionRegister(CompetitionRegister $competitionRegister): self
    {
        if ($this->competitionRegisters->removeElement($competitionRegister)) {
            // set the owning side to null (unless already changed)
            if ($competitionRegister->getCompetition() === $this) {
                $competitionRegister->setCompetition(null);
            }
        }

        return $this;
    }
}
