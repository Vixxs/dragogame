<?php

namespace App\Entity;

use App\Repository\HorseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ManagerRegistry;
use PharIo\Manifest\Manifest;

/**
 * @ORM\Entity(repositoryClass=HorseRepository::class)
 */
class Horse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $race;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $resistance;

    /**
     * @ORM\Column(type="integer")
     */
    private $endurance;

    /**
     * @ORM\Column(type="integer")
     */
    private $jump;

    /**
     * @ORM\Column(type="integer")
     */
    private $speed;

    /**
     * @ORM\Column(type="integer")
     */
    private $sociability;

    /**
     * @ORM\Column(type="integer")
     */
    private $intelligence;

    /**
     * @ORM\Column(type="integer")
     */
    private $temperament;

    /**
     * @ORM\Column(type="integer")
     */
    private $moral_state;

    /**
     * @ORM\Column(type="integer")
     */
    private $health_state;

    /**
     * @ORM\Column(type="integer")
     */
    private $stressfull_state;

    /**
     * @ORM\Column(type="integer")
     */
    private $tired_state;

    /**
     * @ORM\Column(type="integer")
     */
    private $hunger_state;

    /**
     * @ORM\Column(type="integer")
     */
    private $clean_state;

    /**
     * @ORM\Column(type="integer")
     */
    private $general_state;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="integer")
     */
    private $experience;

    /**
     * @ORM\OneToMany(targetEntity=HorseItem::class, mappedBy="horse")
     */
    private $horseItems;

    /**
     * @ORM\OneToMany(targetEntity=HorseInjury::class, mappedBy="horse")
     */
    private $horseInjuries;

    /**
     * @ORM\OneToMany(targetEntity=HorseDisease::class, mappedBy="horse")
     */
    private $horseDiseases;

    /**
     * @ORM\OneToMany(targetEntity=HorseParasite::class, mappedBy="horse")
     */
    private $horseParasites;

    /**
     * @ORM\OneToOne(targetEntity=GlobalObject::class, cascade={"persist", "remove"})
     */
    private $object;

    public function __construct()
    {
        $this->horseItems = new ArrayCollection();
        $this->horseInjuries = new ArrayCollection();
        $this->horseDiseases = new ArrayCollection();
        $this->horseParasites = new ArrayCollection();
        $this->object = new GlobalObject();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRace(): ?string
    {
        return $this->race;
    }

    public function setRace(string $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getResistance(): ?int
    {
        return $this->resistance;
    }

    public function setResistance(int $resistance): self
    {
        $this->resistance = $resistance;

        return $this;
    }

    public function getEndurance(): ?int
    {
        return $this->endurance;
    }

    public function setEndurance(int $endurance): self
    {
        $this->endurance = $endurance;

        return $this;
    }

    public function getJump(): ?int
    {
        return $this->jump;
    }

    public function setJump(int $jump): self
    {
        $this->jump = $jump;

        return $this;
    }

    public function getSpeed(): ?int
    {
        return $this->speed;
    }

    public function setSpeed(int $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    public function getSociability(): ?int
    {
        return $this->sociability;
    }

    public function setSociability(int $sociability): self
    {
        $this->sociability = $sociability;

        return $this;
    }

    public function getIntelligence(): ?int
    {
        return $this->intelligence;
    }

    public function setIntelligence(int $intelligence): self
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    public function getTemperament(): ?int
    {
        return $this->temperament;
    }

    public function setTemperament(int $temperament): self
    {
        $this->temperament = $temperament;

        return $this;
    }

    public function getMoralState(): ?int
    {
        return $this->moral_state;
    }

    public function setMoralState(int $moral_state): self
    {
        $this->moral_state = $moral_state;

        return $this;
    }

    public function getHealthState(): ?int
    {
        return $this->health_state;
    }

    public function setHealthState(int $health_state): self
    {
        $this->health_state = $health_state;

        return $this;
    }

    public function getStressfullState(): ?int
    {
        return $this->stressfull_state;
    }

    public function setStressfullState(int $stressfull_state): self
    {
        $this->stressfull_state = $stressfull_state;

        return $this;
    }

    public function getTiredState(): ?int
    {
        return $this->tired_state;
    }

    public function setTiredState(int $tired_state): self
    {
        $this->tired_state = $tired_state;

        return $this;
    }

    public function getHungerState(): ?int
    {
        return $this->hunger_state;
    }

    public function setHungerState(int $hunger_state): self
    {
        $this->hunger_state = $hunger_state;

        return $this;
    }

    public function getCleanState(): ?int
    {
        return $this->clean_state;
    }

    public function setCleanState(int $clean_state): self
    {
        $this->clean_state = $clean_state;

        return $this;
    }

    public function getGeneralState(): ?int
    {
        return $this->general_state;
    }

    public function setGeneralState(int $general_state): self
    {
        $this->general_state = $general_state;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getExperience(): ?int
    {
        return $this->experience;
    }

    public function setExperience(int $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * @return Collection<int, HorseItem>
     */
    public function getHorseItems(): Collection
    {
        return $this->horseItems;
    }

    public function addHorseItem(HorseItem $horseItem): self
    {
        if (!$this->horseItems->contains($horseItem)) {
            $this->horseItems[] = $horseItem;
            $horseItem->setHorse($this);
        }

        return $this;
    }

    public function removeHorseItem(HorseItem $horseItem): self
    {
        if ($this->horseItems->removeElement($horseItem)) {
            // set the owning side to null (unless already changed)
            if ($horseItem->getHorse() === $this) {
                $horseItem->setHorse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, HorseInjury>
     */
    public function getHorseInjuries(): Collection
    {
        return $this->horseInjuries;
    }

    public function addHorseInjury(HorseInjury $horseInjury): self
    {
        if (!$this->horseInjuries->contains($horseInjury)) {
            $this->horseInjuries[] = $horseInjury;
            $horseInjury->setHorse($this);
        }

        return $this;
    }

    public function removeHorseInjury(HorseInjury $horseInjury): self
    {
        if ($this->horseInjuries->removeElement($horseInjury)) {
            // set the owning side to null (unless already changed)
            if ($horseInjury->getHorse() === $this) {
                $horseInjury->setHorse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, HorseDisease>
     */
    public function getHorseDiseases(): Collection
    {
        return $this->horseDiseases;
    }

    public function addHorseDisease(HorseDisease $horseDisease): self
    {
        if (!$this->horseDiseases->contains($horseDisease)) {
            $this->horseDiseases[] = $horseDisease;
            $horseDisease->setHorse($this);
        }

        return $this;
    }

    public function removeHorseDisease(HorseDisease $horseDisease): self
    {
        if ($this->horseDiseases->removeElement($horseDisease)) {
            // set the owning side to null (unless already changed)
            if ($horseDisease->getHorse() === $this) {
                $horseDisease->setHorse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, HorseParasite>
     */
    public function getHorseParasites(): Collection
    {
        return $this->horseParasites;
    }

    public function addHorseParasite(HorseParasite $horseParasite): self
    {
        if (!$this->horseParasites->contains($horseParasite)) {
            $this->horseParasites[] = $horseParasite;
            $horseParasite->setHorse($this);
        }

        return $this;
    }

    public function removeHorseParasite(HorseParasite $horseParasite): self
    {
        if ($this->horseParasites->removeElement($horseParasite)) {
            // set the owning side to null (unless already changed)
            if ($horseParasite->getHorse() === $this) {
                $horseParasite->setHorse(null);
            }
        }

        return $this;
    }

    public function getObject(): ?GlobalObject
    {
        return $this->object;
    }

    public function setObject(?GlobalObject $object): self
    {
        $this->object = $object;

        return $this;
    }
}
