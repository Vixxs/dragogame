<?php

namespace App\Entity;

use App\Repository\HorseDiseaseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HorseDiseaseRepository::class)
 */
class HorseDisease
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Horse::class, inversedBy="horseDiseases")
     */
    private $horse;

    /**
     * @ORM\ManyToOne(targetEntity=Disease::class, inversedBy="horseDiseases")
     */
    private $disease;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHorse(): ?Horse
    {
        return $this->horse;
    }

    public function setHorse(?Horse $horse): self
    {
        $this->horse = $horse;

        return $this;
    }

    public function getDisease(): ?Disease
    {
        return $this->disease;
    }

    public function setDisease(?Disease $disease): self
    {
        $this->disease = $disease;

        return $this;
    }
}
