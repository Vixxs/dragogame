<?php

namespace App\Entity;

use App\Repository\StableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @ORM\Entity(repositoryClass=StableRepository::class)
 */
class Stable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacity;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="stables")
     */
    private $account;

    /**
     * @ORM\OneToMany(targetEntity=StableInfra::class, mappedBy="stable")
     */
    private $stableInfras;

    /**
     * @ORM\OneToOne(targetEntity=GlobalObject::class, cascade={"persist", "remove"})
     */
    private $object;

    public function __construct()
    {
        $this->stableInfras = new ArrayCollection();
        $this->object = new GlobalObject();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Collection<int, StableInfra>
     */
    public function getStableInfras(): Collection
    {
        return $this->stableInfras;
    }

    public function addStableInfra(StableInfra $stableInfra): self
    {
        if (!$this->stableInfras->contains($stableInfra)) {
            $this->stableInfras[] = $stableInfra;
            $stableInfra->setStable($this);
        }

        return $this;
    }

    public function removeStableInfra(StableInfra $stableInfra): self
    {
        if ($this->stableInfras->removeElement($stableInfra)) {
            // set the owning side to null (unless already changed)
            if ($stableInfra->getStable() === $this) {
                $stableInfra->setStable(null);
            }
        }

        return $this;
    }

    public function getObject(): ?GlobalObject
    {
        return $this->object;
    }

    public function setObject(?GlobalObject $object): self
    {
        $this->object = $object;

        return $this;
    }
}
