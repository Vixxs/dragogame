<?php

namespace App\Entity;

use App\Repository\NewspaperDiversRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewspaperDiversRepository::class)
 */
class NewspaperDivers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Newspaper::class, inversedBy="newspaperDivers")
     */
    private $newspaper;

    /**
     * @ORM\ManyToOne(targetEntity=Divers::class, inversedBy="newspaperDivers")
     */
    private $divers;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNewspaper(): ?Newspaper
    {
        return $this->newspaper;
    }

    public function setNewspaper(?Newspaper $newspaper): self
    {
        $this->newspaper = $newspaper;

        return $this;
    }

    public function getDivers(): ?Divers
    {
        return $this->divers;
    }

    public function setDivers(?Divers $divers): self
    {
        $this->divers = $divers;

        return $this;
    }
}
