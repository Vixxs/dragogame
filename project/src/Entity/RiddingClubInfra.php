<?php

namespace App\Entity;

use App\Repository\RiddingClubInfraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RiddingClubInfraRepository::class)
 */
class RiddingClubInfra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=RiddingClub::class, inversedBy="riddingClubInfras")
     */
    private $riddingClub;

    /**
     * @ORM\OneToOne(targetEntity=Infrastructure::class, cascade={"persist", "remove"})
     */
    private $infrastructure;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRiddingClub(): ?RiddingClub
    {
        return $this->riddingClub;
    }

    public function setRiddingClub(?RiddingClub $riddingClub): self
    {
        $this->riddingClub = $riddingClub;

        return $this;
    }

    public function getInfrastructure(): ?Infrastructure
    {
        return $this->infrastructure;
    }

    public function setInfrastructure(?Infrastructure $infrastructure): self
    {
        $this->infrastructure = $infrastructure;

        return $this;
    }
}
