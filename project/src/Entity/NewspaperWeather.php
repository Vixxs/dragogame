<?php

namespace App\Entity;

use App\Repository\NewspaperWeatherRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewspaperWeatherRepository::class)
 */
class NewspaperWeather
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Newspaper::class, inversedBy="newspaperWeather")
     */
    private $newspaper;

    /**
     * @ORM\ManyToOne(targetEntity=Weather::class, inversedBy="newspaperWeather")
     */
    private $weather;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNewspaper(): ?Newspaper
    {
        return $this->newspaper;
    }

    public function setNewspaper(?Newspaper $newspaper): self
    {
        $this->newspaper = $newspaper;

        return $this;
    }

    public function getWeather(): ?Weather
    {
        return $this->weather;
    }

    public function setWeather(?Weather $weather): self
    {
        $this->weather = $weather;

        return $this;
    }
}
