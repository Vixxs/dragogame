<?php

namespace App\Entity;

use App\Repository\RiddingClubRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity(repositoryClass=RiddingClubRepository::class)
*/
class RiddingClub
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $subscription;

    /**
     * @ORM\OneToMany(mappedBy="id_club", targetEntity=Competition::class, orphanRemoval=true)
     */
    private $competitions;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="riddingClubs")
     */
    private $account;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacity;

    /**
     * @ORM\OneToMany(targetEntity=RiddingClubMember::class, mappedBy="riddingClub")
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity=RiddingClubInfra::class, mappedBy="riddingClub")
     */
    private $riddingClubInfras;

    public function __construct()
    {
        $this->id_account = new ArrayCollection();
        $this->competitions = new ArrayCollection();
        $this->members = new ArrayCollection();
        $this->riddingClubInfras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getSubscription(): ?float
    {
        return $this->subscription;
    }

    public function setSubscription(float $subscription): self
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * @return Collection<int, Competition>
     */
    public function getCompetitions(): Collection
    {
        return $this->competitions;
    }

    public function addCompetition(Competition $competition): self
    {
        if (!$this->competitions->contains($competition)) {
            $this->competitions[] = $competition;
            $competition->setIdClub($this);
        }

        return $this;
    }

    public function removeCompetition(Competition $competition): self
    {
        if ($this->competitions->removeElement($competition)) {
            // set the owning side to null (unless already changed)
            if ($competition->getIdClub() === $this) {
                $competition->setIdClub(null);
            }
        }

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return Collection<int, RiddingClubMember>
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(RiddingClubMember $member): self
    {
        if (!$this->members->contains($member)) {
            $this->members[] = $member;
            $member->setRiddingClub($this);
        }

        return $this;
    }

    public function removeMember(RiddingClubMember $member): self
    {
        if ($this->members->removeElement($member)) {
            // set the owning side to null (unless already changed)
            if ($member->getRiddingClub() === $this) {
                $member->setRiddingClub(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RiddingClubInfra>
     */
    public function getRiddingClubInfras(): Collection
    {
        return $this->riddingClubInfras;
    }

    public function addRiddingClubInfra(RiddingClubInfra $riddingClubInfra): self
    {
        if (!$this->riddingClubInfras->contains($riddingClubInfra)) {
            $this->riddingClubInfras[] = $riddingClubInfra;
            $riddingClubInfra->setRiddingClub($this);
        }

        return $this;
    }

    public function removeRiddingClubInfra(RiddingClubInfra $riddingClubInfra): self
    {
        if ($this->riddingClubInfras->removeElement($riddingClubInfra)) {
            // set the owning side to null (unless already changed)
            if ($riddingClubInfra->getRiddingClub() === $this) {
                $riddingClubInfra->setRiddingClub(null);
            }
        }

        return $this;
    }
}
