<?php

namespace App\Entity;

use App\Repository\HorseInjuryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HorseInjuryRepository::class)
 */
class HorseInjury
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Horse::class, inversedBy="horseInjuries")
     */
    private $horse;

    /**
     * @ORM\ManyToOne(targetEntity=Injury::class, inversedBy="horseInjuries")
     */
    private $injury;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHorse(): ?Horse
    {
        return $this->horse;
    }

    public function setHorse(?Horse $horse): self
    {
        $this->horse = $horse;

        return $this;
    }

    public function getInjury(): ?Injury
    {
        return $this->injury;
    }

    public function setInjury(?Injury $injury): self
    {
        $this->injury = $injury;

        return $this;
    }
}
