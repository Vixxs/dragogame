<?php

namespace App\Entity;

use App\Repository\StableInfraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StableInfraRepository::class)
 */
class StableInfra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Stable::class, inversedBy="stableInfras")
     */
    private $stable;

    /**
     * @ORM\OneToOne(targetEntity=Infrastructure::class, cascade={"persist", "remove"})
     */
    private $infrastructure;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStable(): ?Stable
    {
        return $this->stable;
    }

    public function setStable(?Stable $stable): self
    {
        $this->stable = $stable;

        return $this;
    }

    public function getInfrastructure(): ?Infrastructure
    {
        return $this->infrastructure;
    }

    public function setInfrastructure(?Infrastructure $infrastructure): self
    {
        $this->infrastructure = $infrastructure;

        return $this;
    }
}
