<?php

namespace App\Entity;

use App\Repository\InfrastructureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InfrastructureRepository::class)
 */
class Infrastructure
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $ressources_cosumption;

    /**
     * @ORM\Column(type="integer")
     */
    private $horse_capacity;

    /**
     * @ORM\Column(type="integer")
     */
    private $item_capacity;

    /**
     * @ORM\OneToMany(targetEntity=InfrastructureItem::class, mappedBy="infrastructure")
     */
    private $infrastructureItems;

    public function __construct()
    {
        $this->infrastructureItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFamily(): ?string
    {
        return $this->family;
    }

    public function setFamily(string $family): self
    {
        $this->family = $family;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRessourcesCosumption(): ?int
    {
        return $this->ressources_cosumption;
    }

    public function setRessourcesCosumption(int $ressources_cosumption): self
    {
        $this->ressources_cosumption = $ressources_cosumption;

        return $this;
    }

    public function getHorseCapacity(): ?int
    {
        return $this->horse_capacity;
    }

    public function setHorseCapacity(int $horse_capacity): self
    {
        $this->horse_capacity = $horse_capacity;

        return $this;
    }

    public function getItemCapacity(): ?int
    {
        return $this->item_capacity;
    }

    public function setItemCapacity(int $item_capacity): self
    {
        $this->item_capacity = $item_capacity;

        return $this;
    }

    /**
     * @return Collection<int, InfrastructureItem>
     */
    public function getInfrastructureItems(): Collection
    {
        return $this->infrastructureItems;
    }

    public function addInfrastructureItem(InfrastructureItem $infrastructureItem): self
    {
        if (!$this->infrastructureItems->contains($infrastructureItem)) {
            $this->infrastructureItems[] = $infrastructureItem;
            $infrastructureItem->setInfrastructure($this);
        }

        return $this;
    }

    public function removeInfrastructureItem(InfrastructureItem $infrastructureItem): self
    {
        if ($this->infrastructureItems->removeElement($infrastructureItem)) {
            // set the owning side to null (unless already changed)
            if ($infrastructureItem->getInfrastructure() === $this) {
                $infrastructureItem->setInfrastructure(null);
            }
        }

        return $this;
    }
}
