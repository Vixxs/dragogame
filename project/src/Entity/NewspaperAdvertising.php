<?php

namespace App\Entity;

use App\Repository\NewspaperAdvertisingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewspaperAdvertisingRepository::class)
 */
class NewspaperAdvertising
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Newspaper::class, inversedBy="newspaperAdvertisings")
     */
    private $newspaper;

    /**
     * @ORM\ManyToOne(targetEntity=Advertising::class, inversedBy="newspaperAdvertisings")
     */
    private $advertising;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNewspaper(): ?Newspaper
    {
        return $this->newspaper;
    }

    public function setNewspaper(?Newspaper $newspaper): self
    {
        $this->newspaper = $newspaper;

        return $this;
    }

    public function getAdvertising(): ?Advertising
    {
        return $this->advertising;
    }

    public function setAdvertising(?Advertising $advertising): self
    {
        $this->advertising = $advertising;

        return $this;
    }
}
