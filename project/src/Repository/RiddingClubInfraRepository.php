<?php

namespace App\Repository;

use App\Entity\RiddingClubInfra;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RiddingClubInfra|null find($id, $lockMode = null, $lockVersion = null)
 * @method RiddingClubInfra|null findOneBy(array $criteria, array $orderBy = null)
 * @method RiddingClubInfra[]    findAll()
 * @method RiddingClubInfra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RiddingClubInfraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RiddingClubInfra::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(RiddingClubInfra $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(RiddingClubInfra $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return RiddingClubInfra[] Returns an array of RiddingClubInfra objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RiddingClubInfra
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
