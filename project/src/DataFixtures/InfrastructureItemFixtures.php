<?php

namespace App\DataFixtures;

use App\Entity\Infrastructure;
use App\Entity\InfrastructureItem;
use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class InfrastructureItemFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $repository = $manager->getRepository(InfrastructureItem::class);
        $infraRepository = $manager->getRepository(Infrastructure::class);
        $itemRepository = $manager->getRepository(Item::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $infras = $infraRepository->findAll();
            $infra = $infras[array_rand($infras)];
            $items = $itemRepository->findAll();
            $item = $items[array_rand($items)];

            while ($repository->findOneBy(['item' => $item]) !== null) {
                $item = $items[array_rand($items)];
            }

            $horseitem = new InfrastructureItem();
            $horseitem->setInfrastructure($infra);
            $horseitem->setItem($item);
            $manager->persist($horseitem);
            $manager->flush();
            if (($i % $batchSize) === 0) {
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            InfrastructureFixtures::class,
            ItemFixtures::class,
        ];
    }
}