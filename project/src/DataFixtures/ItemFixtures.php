<?php

namespace App\DataFixtures;

use App\Entity\Item;
use App\Entity\ItemFamily;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
class ItemFixtures extends Fixture implements DependentFixtureInterface

{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        $repository = $manager->getRepository(ItemFamily::class);
        for ($i = 0; $i < 1000; $i++) {
            $familys = $repository->findAll();
            $family = $familys[array_rand($familys)];
            $item = new Item();
            $item->setFamily($family);
            $item->setType(ucfirst($faker->word(1)));
            $item->setLevel($faker->numberBetween(1, 10));
            $item->setDescription($faker->text());
            $item->setPrice($faker->numberBetween(1, 100));
            $manager->persist($item);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            ItemFamilyFixtures::class,
        ];
    }

}
