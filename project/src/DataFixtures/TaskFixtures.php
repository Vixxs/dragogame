<?php

namespace App\DataFixtures;

use App\Entity\GlobalObject;
use App\Entity\Planning;
use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TaskFixtures extends Fixture implements DependentFixtureInterface

{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        $objectRepository = $manager->getRepository(GlobalObject::class);
        $planningRepository = $manager->getRepository(Planning::class);
        for ($i = 0; $i < 1000; $i++) {
            $objects = $objectRepository->findAll();
            $object = $objects[array_rand($objects)];
            $plannings = $planningRepository->findAll();
            $planning = $plannings[array_rand($plannings)];

            $task = new Task();
            $task->setObject($object);
            $task->setPlanning($planning);
            $task->setAction(ucfirst($faker->word(1)));
            $task->setFrequency($faker->numberBetween(1, 10));
            $manager->persist($task);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            PlanningFixtures::class,
        ];
    }
}