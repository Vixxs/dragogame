<?php

namespace App\DataFixtures;

use App\Entity\Infrastructure;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
class InfrastructureFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $infrastructure = new Infrastructure();
            $infrastructure->setType(ucfirst($faker->word(1)));
            $infrastructure->setLevel($faker->numberBetween(1, 5));
            $infrastructure->setDescription($faker->text());
            $infrastructure->setPrice($faker->numberBetween(1, 100));
            $infrastructure->setRessourcesCosumption($faker->numberBetween(1, 1000));
            $infrastructure->setHorseCapacity($faker->numberBetween(1, 10));
            $infrastructure->setItemCapacity($faker->numberBetween(1, 30));
            $manager->persist($infrastructure);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            AccountFixtures::class,
        ];
    }


}
