<?php

namespace App\DataFixtures;

use App\Entity\Weather;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class WeatherFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $weather = new Weather();
            $weather->setTitle(ucfirst($faker->word(1)));
            $weather->setText($faker->text());
            $weather->setImage($faker->imageUrl());
            $manager->persist($weather);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
    
}