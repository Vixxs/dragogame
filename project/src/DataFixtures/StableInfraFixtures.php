<?php

namespace App\DataFixtures;

use App\Entity\Infrastructure;
use App\Entity\Stable;
use App\Entity\StableInfra;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
class StableInfraFixtures extends Fixture implements DependentFixtureInterface

{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        $repository = $manager->getRepository(StableInfra::class);
        $stableRepository = $manager->getRepository(Stable::class);
        $infraRepository = $manager->getRepository(Infrastructure::class);
        for ($i = 0; $i < 1000; $i++) {
            $stables = $stableRepository->findAll();
            $stable = $stables[array_rand($stables)];
            $infras = $infraRepository->findAll();
            $infra = $infras[array_rand($infras)];

            while ($repository->findOneBy(['infrastructure' => $infra]) !== null) {
                $infra = $infras[array_rand($infras)];
            }

            $stableInfra = new StableInfra();
            $stableInfra->setInfrastructure($infra);
            $stableInfra->setStable($stable);
            $manager->persist($stableInfra);
            $manager->flush();
            if (($i % $batchSize) === 0) {
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            StableFixtures::class,
            InfrastructureFixtures::class,
        ];
    }

}