<?php

namespace App\DataFixtures;

use App\Entity\Horse;
use App\Entity\HorseItem;
use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class HorseItemFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $repository = $manager->getRepository(HorseItem::class);
        $horseRepository = $manager->getRepository(Horse::class);
        $itemRepository = $manager->getRepository(Item::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $horses = $horseRepository->findAll();
            $horse = $horses[array_rand($horses)];
            $items = $itemRepository->findAll();
            $item = $items[array_rand($items)];

            while ($repository->findOneBy(['item' => $item]) !== null) {
                $item = $items[array_rand($items)];
            }

            $horseitem = new HorseItem();
            $horseitem->setHorse($horse);
            $horseitem->setItem($item);
            $manager->persist($horseitem);
            $manager->flush();
            if (($i % $batchSize) === 0) {
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            HorseFixtures::class,
            ItemFixtures::class,
        ];
    }
}