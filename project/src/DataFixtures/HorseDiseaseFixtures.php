<?php

namespace App\DataFixtures;

use App\Entity\Disease;
use App\Entity\Horse;
use App\Entity\HorseDisease;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class HorseDiseaseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $horseRepository = $manager->getRepository(Horse::class);
        $diseaseRepository = $manager->getRepository(Disease::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $horsedisease = new HorseDisease();
            $horses = $horseRepository->findAll();
            $horse = $horses[array_rand($horses)];
            $diseases = $diseaseRepository->findAll();
            $disease = $diseases[array_rand($diseases)];
            $horsedisease->setHorse($horse);
            $horsedisease->setDisease($disease);
            $manager->persist($horsedisease);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            HorseFixtures::class,
            DiseaseFixtures::class,
        ];
    }
}