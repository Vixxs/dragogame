<?php

namespace App\DataFixtures;

use App\Entity\Injury;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class InjuryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $injury = new Injury();
            $injury->setName(ucfirst($faker->word(1)));
            $manager->persist($injury);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
}
