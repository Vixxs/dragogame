<?php

namespace App\DataFixtures;

use App\Entity\Infrastructure;
use App\Entity\RiddingClub;
use App\Entity\RiddingClubInfra;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RiddingClubInfraFixtures extends Fixture implements DependentFixtureInterface

{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        $repository = $manager->getRepository(RiddingClubInfra::class);
        $clubRepository = $manager->getRepository(RiddingClub::class);
        $infraRepository = $manager->getRepository(Infrastructure::class);

        for ($i = 0; $i < 1000; $i++) {
            $riddingClubs = $clubRepository->findAll();
            $riddingClub = $riddingClubs[array_rand($riddingClubs)];
            $infras = $infraRepository->findAll();
            $infra = $infras[array_rand($infras)];
            while ($repository->findOneBy(['infrastructure' => $infra]) !== null) {
                $infra = $infras[array_rand($infras)];
            }

            $riddingClubInfra = new RiddingClubInfra();
            $riddingClubInfra->setInfrastructure($infra);
            $riddingClubInfra->setRiddingClub($riddingClub);
            $manager->persist($riddingClubInfra);
            $manager->flush();
            if (($i % $batchSize) === 0) {
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            RiddingClubFixtures::class,
            InfrastructureFixtures::class,
        ];
    }

}