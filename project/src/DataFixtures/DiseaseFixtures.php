<?php

namespace App\DataFixtures;

use App\Entity\Disease;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DiseaseFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;

        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $disease = new Disease();
            $disease->setName(ucfirst($faker->word(1)));
            $manager->persist($disease);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
}
