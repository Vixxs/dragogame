<?php

namespace App\DataFixtures;

use App\Entity\Circuit;
use App\Entity\Infrastructure;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CircuitFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create('fr_FR');
        $batchSize = 20;
        $repository = $manager->getRepository(Infrastructure::class);
        $circuitRepository = $manager->getRepository(Circuit::class);
        for ($i = 0; $i < 1000; $i++) {
            $circuit = new Circuit();
            $circuit->setName(ucfirst($faker->word(1)));
            $elements = $repository->findAll();
            $element = $elements[array_rand($elements)];
            while ($circuitRepository->findOneBy(['id_infra' => $element]) !== null) {
                $element = $elements[array_rand($elements)];
            } 
            $circuit->setIdInfra($element);
            $manager->persist($circuit);
            $manager->flush();
            if (($i % $batchSize) === 0) {
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            InfrastructureFixtures::class
        ];
    }
}
