<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\BanqHistory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class BankFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;

        $faker = \Faker\Factory::create('fr_FR');
        $accountRepository = $manager->getRepository(Account::class);
        for ($i = 0; $i < 1000; $i++) {
            //get random account
            $accounts = $accountRepository->findAll();
            $account = $accounts[array_rand($accounts)];
            $banqHistory = new BanqHistory();
            $banqHistory->setAccount($account);
            $banqHistory->setTransaction($faker->numberBetween(-100, 100));
            $banqHistory->setDate($faker->dateTimeBetween('-60 years', '-18 years'));
            $manager->persist($banqHistory);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            AccountFixtures::class,
        ];
    }

}
