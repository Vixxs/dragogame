<?php

namespace App\DataFixtures;

use App\Entity\Horse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class HorseFixtures extends Fixture
{
    const HORSE_RACE = [
        "Prune",
        "Indigo",
        "Ebene",
        "Ivoire",
        "Rousse",
        "Emeraude",
        "Orchidee",
        "Amande",
        "Doré",
        "Turquoise",
        "Pourpre"
    ];

    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 10000; $i++) {
            $horse = new Horse();
            $horse->setName($faker->firstName());
            $horse->setRace($faker->randomElement(self::HORSE_RACE));
            $horse->setDescription($faker->text());
            $horse->setResistance($faker->numberBetween(1,10));
            $horse->setEndurance($faker->numberBetween(1,10));
            $horse->setJump($faker->numberBetween(1,10));
            $horse->setSpeed($faker->numberBetween(1,10));
            $horse->setSociability($faker->numberBetween(1,10));
            $horse->setIntelligence($faker->numberBetween(1,10));
            $horse->setTemperament($faker->numberBetween(1,10));
            $horse->setMoralState($faker->numberBetween(1,10));
            $horse->setHealthState($faker->numberBetween(1,10));
            $horse->setStressfullState($faker->numberBetween(1,10));
            $horse->setTiredState($faker->numberBetween(1,10));
            $horse->setHungerState($faker->numberBetween(1,10));
            $horse->setCleanState($faker->numberBetween(1,10));
            $horse->setGeneralState($faker->numberBetween(1,10));
            $horse->setLevel($faker->numberBetween(1,10));
            $horse->setExperience($faker->numberBetween(1,10));
            $manager->persist($horse);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
}
