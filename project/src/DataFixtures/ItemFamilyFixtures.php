<?php

namespace App\DataFixtures;

use App\Entity\ItemFamily;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ItemFamilyFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $itemFamily = new ItemFamily();
            $itemFamily->setLabel(ucfirst($faker->word(1)));
            $manager->persist($itemFamily);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            AccountFixtures::class,
        ];
    }


}
