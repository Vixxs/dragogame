<?php

namespace App\DataFixtures;

use App\Entity\News;
use App\Entity\Newspaper;
use App\Entity\NewspaperNews;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
class NewspaperNewsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $newspaperRepository = $manager->getRepository(Newspaper::class);
        $advertisingRepository = $manager->getRepository(News::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $newspapernews = new NewspaperNews();
            $newss = $newspaperRepository->findAll();
            $news = $newss[array_rand($newss)];
            $nes = $advertisingRepository->findAll();
            $ne = $nes[array_rand($nes)];
            $newspapernews->setNewspaper($news);
            $newspapernews->setNews($ne);
            $manager->persist($newspapernews);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            NewspaperFixtures::class,
            NewsFixtures::class,
        ];
    }

}