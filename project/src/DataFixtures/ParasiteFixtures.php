<?php

namespace App\DataFixtures;

use App\Entity\Divers;
use App\Entity\Parasite;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ParasiteFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $parasite = new Parasite();
            $parasite->setName(ucfirst($faker->word(1)));
            $manager->persist($parasite);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
}