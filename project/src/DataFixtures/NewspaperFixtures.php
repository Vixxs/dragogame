<?php

namespace App\DataFixtures;

use App\Entity\Newspaper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class NewspaperFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $newspaper = new Newspaper();
            $newspaper->setDate($faker->dateTimeBetween('-1 years', 'now'));
            $newspaper->setKeyPoint($faker->sentence(3));
            $manager->persist($newspaper);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
}