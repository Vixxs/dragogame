<?php

namespace App\DataFixtures;

use App\Entity\Competition;
use App\Entity\CompetitionItem;
use App\Entity\Item;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CompetitionItemFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;

        $repository = $manager->getRepository(CompetitionItem::class);
        $itemRepository = $manager->getRepository(Item::class);
        $competitiontRepository = $manager->getRepository(Competition::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $competitionitem = new CompetitionItem();
            $items = $itemRepository->findAll();
            $item = $items[array_rand($items)];
            $competitions = $competitiontRepository->findAll();
            $competition = $competitions[array_rand($competitions)];

            while ($repository->findOneBy(['item' => $item]) !== null) {
                $item = $items[array_rand($items)];
            }

            $competitionitem->setItem($item);
            $competitionitem->setCompetition($competition);
            $manager->persist($competitionitem);
            $manager->flush();
            if (($i % $batchSize) === 0) {
                $manager->clear();
            }
        }
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            ItemFixtures::class,
            CompetitionFixtures::class,
        ];
    }
}