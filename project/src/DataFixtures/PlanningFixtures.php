<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Planning;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PlanningFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        $accountRepository = $manager->getRepository(Account::class);
        for ($i = 0; $i < 1000; $i++) {
            $planning = new Planning();
            $accounts = $accountRepository->findAll();
            $account = $accounts[array_rand($accounts)];
            $planning->setAccount($account);
            $manager->persist($planning);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            } 
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            AccountFixtures::class,
        ];
    }

}