<?php

namespace App\DataFixtures;

use App\Entity\Newspaper;
use App\Entity\NewspaperWeather;
use App\Entity\Weather;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class NewspaperWeatherFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $newspaperRepository = $manager->getRepository(Newspaper::class);
        $weatherRepository = $manager->getRepository(Weather::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $newspaperaweather = new NewspaperWeather();
            $newss = $newspaperRepository->findAll();
            $news = $newss[array_rand($newss)];
            $weathers = $weatherRepository->findAll();
            $weather = $weathers[array_rand($weathers)];
            $newspaperaweather->setNewspaper($news);
            $newspaperaweather->setWeather($weather);
            $manager->persist($newspaperaweather);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            NewspaperFixtures::class,
            WeatherFixtures::class,
        ];
    }

}