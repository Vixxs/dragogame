<?php

namespace App\DataFixtures;

use App\Entity\Horse;
use App\Entity\HorseInjury;
use App\Entity\Injury;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class HorseInjuryFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $horseRepository = $manager->getRepository(Horse::class);
        $injuryRepository = $manager->getRepository(Injury::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $horseinjury = new HorseInjury();
            $horses = $horseRepository->findAll();
            $horse = $horses[array_rand($horses)];
            $injuries = $injuryRepository->findAll();
            $injury = $injuries[array_rand($injuries)];
            $horseinjury->setHorse($horse);
            $horseinjury->setInjury($injury);
            $manager->persist($horseinjury);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            HorseFixtures::class,
            InjuryFixtures::class,
        ];
    }
}