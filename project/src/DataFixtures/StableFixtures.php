<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Stable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
class StableFixtures extends Fixture implements DependentFixtureInterface

{
    public function load(ObjectManager $manager): void 
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        $accountRepository = $manager->getRepository(Account::class);
        for ($i = 0; $i < 1000; $i++) {
            $stable = new Stable();
            $accounts = $accountRepository->findAll();
            $account = $accounts[array_rand($accounts)];
            $stable->setAccount($account);
            $stable->setCapacity($faker->numberBetween(1, 10));
            $manager->persist($stable);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
    

    public function getDependencies()
    {
        return [
            AccountFixtures::class,
        ];
    }
  
}