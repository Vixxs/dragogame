<?php

namespace App\DataFixtures;

use App\Entity\Advertising;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
class AdvertisingFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;

        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $advertising = new Advertising();
            $advertising->setTitle(ucfirst($faker->word(1)));
            $advertising->setText($faker->text());
            $advertising->setImage($faker->imageUrl());
            $manager->persist($advertising);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
}
