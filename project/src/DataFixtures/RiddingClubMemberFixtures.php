<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\RiddingClub;
use App\Entity\RiddingClubMember;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RiddingClubMemberFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        $clubRepository = $manager->getRepository(RiddingClub::class);
        $memberRepository = $manager->getRepository(Account::class);
        for ($i = 0; $i < 1000; $i++) {
            $riddingClubs = $clubRepository->findAll();
            $riddingClub = $riddingClubs[array_rand($riddingClubs)];
            $members = $memberRepository->findAll();
            $member = $members[array_rand($members)];

            $riddingClubMember = new RiddingClubMember();
            $riddingClubMember->setAccount($member);
            $riddingClubMember->setRiddingClub($riddingClub);
            $manager->persist($riddingClubMember);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            RiddingClubFixtures::class,
            AccountFixtures::class,
        ];
    }

}