<?php

namespace App\DataFixtures;

use App\Entity\Competition;
use App\Entity\RiddingClub;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CompetitionFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;

        $clubRepository = $manager->getRepository(RiddingClub::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $competition = new Competition();
            $clubs = $clubRepository->findAll();
            $club = $clubs[array_rand($clubs)];
            $competition->setIdClub($club);
            $date = $faker->dateTimeBetween('-1 years', '+1 years');
            $dateEnd = $date->modify('+3 days');
            $competition->setDate($date);
            $competition->setDateEnd($dateEnd);
            $subscriberPrice = $faker->randomFloat(2, 0, 100);
            $competition->setSubscriberPrice($subscriberPrice);
            $competition->setPrice($faker->randomFloat(2, $subscriberPrice, $subscriberPrice + 1000));
            $manager->persist($competition);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            RiddingClubFixtures::class,
        ];
    }
}