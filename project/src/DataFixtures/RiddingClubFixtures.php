<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\RiddingClub;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
class RiddingClubFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        $accountRepository = $manager->getRepository(Account::class);
        for ($i = 0; $i < 1000; $i++) {
            $riddingClub = new RiddingClub();
            $accounts = $accountRepository->findAll();
            $account = $accounts[array_rand($accounts)];
            $riddingClub->setAccount($account);
            $riddingClub->setCapacity($faker->numberBetween(1, 10));
            $riddingClub->setSubscription($faker->numberBetween(1, 100));
            $manager->persist($riddingClub);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            AccountFixtures::class,
        ];
    }

}