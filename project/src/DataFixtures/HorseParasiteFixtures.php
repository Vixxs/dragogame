<?php

namespace App\DataFixtures;


use App\Entity\Horse;
use App\Entity\HorseParasite;
use App\Entity\Parasite;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
class HorseParasiteFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $horseRepository = $manager->getRepository(Horse::class);
        $parasiteRepository = $manager->getRepository(Parasite::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $horseaccount = new HorseParasite();
            $horses = $horseRepository->findAll();
            $horse = $horses[array_rand($horses)];
            $parasites = $parasiteRepository->findAll();
            $parasite = $parasites[array_rand($parasites)];
            $horseaccount->setHorse($horse);
            $horseaccount->setParasite($parasite);
            $manager->persist($horseaccount);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            HorseFixtures::class,
            ParasiteFixtures::class,
        ];
    }
}