<?php

namespace App\DataFixtures;

use App\Entity\Divers;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DiversFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $divers = new Divers();
            $divers->setTitle(ucfirst($faker->word(1)));
            $divers->setText($faker->text());
            $divers->setImage($faker->imageUrl());
            $manager->persist($divers);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }
}
