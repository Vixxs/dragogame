<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\Newspaper;
use App\Entity\NewspaperEvent;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
class NewspaperEventFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $newspaperRepository = $manager->getRepository(Newspaper::class);
        $eventRepository = $manager->getRepository(Event::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $newspaperevent = new NewspaperEvent();
            $newss = $newspaperRepository->findAll();
            $news = $newss[array_rand($newss)];
            $events = $eventRepository->findAll();
            $event = $events[array_rand($events)];
            $newspaperevent->setNewspaper($news);
            $newspaperevent->setEvent($event);
            $manager->persist($newspaperevent);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            NewspaperFixtures::class,
            EventFixtures::class,
        ];
    }

}