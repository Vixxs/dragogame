<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Competition;
use App\Entity\CompetitionRegister;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CompetitionRegisterFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;

        $repository = $manager->getRepository(CompetitionRegister::class);
        $itemRepository = $manager->getRepository(Account::class);
        $competitiontRepository = $manager->getRepository(Competition::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $competitionitem = new CompetitionRegister();
            $items = $itemRepository->findAll();
            $item = $items[array_rand($items)];
            $competitions = $competitiontRepository->findAll();
            $competition = $competitions[array_rand($competitions)];

            while ($repository->findOneBy(['account' => $item]) !== null) {
                $item = $items[array_rand($items)];
            }

            $competitionitem->setAccount($item);
            $competitionitem->setCompetition($competition);
            $manager->persist($competitionitem);
            $manager->flush();
            if (($i % $batchSize) === 0) {
                $manager->clear();
            }
        }
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            AccountFixtures::class,
            CompetitionFixtures::class
        ];
    }
}