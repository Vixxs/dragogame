<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Horse;
use App\Entity\HorseAccount;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class HorseAccountFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $horseRepository = $manager->getRepository(Horse::class);
        $accountRepository = $manager->getRepository(Account::class);
        $repository = $manager->getRepository(HorseAccount::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $horseaccount = new HorseAccount();
            $horses = $horseRepository->findAll();
            $horse = $horses[array_rand($horses)];
            $accounts = $accountRepository->findAll();
            $account = $accounts[array_rand($accounts)];

            while ($repository->findOneBy(['horse' => $horse]) !== null) {
                $horse = $horses[array_rand($horses)];
            }
            $horseaccount->setHorse($horse);
            $horseaccount->setAccount($account);
            $manager->persist($horseaccount);
            $manager->flush();
            if (($i % $batchSize) === 0) {
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }

    public function getDependencies()
    {
        return [
            HorseFixtures::class,
            AccountFixtures::class,
        ];
    }
}