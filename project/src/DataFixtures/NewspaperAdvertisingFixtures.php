<?php

namespace App\DataFixtures;

use App\Entity\Advertising;
use App\Entity\Newspaper;
use App\Entity\NewspaperAdvertising;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class NewspaperAdvertisingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $newspaperRepository = $manager->getRepository(Newspaper::class);
        $advertisingRepository = $manager->getRepository(Advertising::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $newspaperadvertising = new NewspaperAdvertising();
            $newss = $newspaperRepository->findAll();
            $news = $newss[array_rand($newss)];
            $parasites = $advertisingRepository->findAll();
            $parasite = $parasites[array_rand($parasites)];
            $newspaperadvertising->setNewspaper($news);
            $newspaperadvertising->setAdvertising($parasite);
            $manager->persist($newspaperadvertising);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            NewspaperFixtures::class,
            AdvertisingFixtures::class,
        ];
    }

}