<?php

namespace App\DataFixtures;

use App\Entity\Divers;
use App\Entity\Newspaper;
use App\Entity\NewspaperDivers;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class NewspaperDiversFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $batchSize = 20;
        $newspaperRepository = $manager->getRepository(Newspaper::class);
        $diversRepository = $manager->getRepository(Divers::class);
        $faker = \Faker\Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $newspaperadvertising = new NewspaperDivers();
            $newss = $newspaperRepository->findAll();
            $news = $newss[array_rand($newss)];
            $divers = $diversRepository->findAll();
            $diver = $divers[array_rand($divers)];
            $newspaperadvertising->setNewspaper($news);
            $newspaperadvertising->setDivers($diver);
            $manager->persist($newspaperadvertising);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        $manager->clear();
    }


    public function getDependencies()
    {
        return [
            NewspaperFixtures::class,
            DiversFixtures::class,
        ];
    }

}