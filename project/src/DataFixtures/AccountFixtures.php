<?php

namespace App\DataFixtures;

use App\Entity\Account;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
class AccountFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create('fr_FR');
        $batchSize = 20;
        for ($i = 0; $i < 1000; $i++) {
            $account = new Account();
            $account->setUsername($faker->unique()->userName());
            $account->setPassword($faker->password());
            $account->setEmail($faker->unique()->email());
            $account->setFirstname($faker->firstName());
            $account->setLastname($faker->lastName());
            $account->setBirthdate($faker->dateTimeBetween('-60 years', '-18 years'));
            $account->setSexe($faker->randomElement(['homme', 'femme'])); 
            $account->setIp($faker->ipv4());
            $account->setRoles(['ROLE_USER']);
            $manager->persist($account);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear();
            }
        }
        $account = new Account();
        $account->setUsername('admin');
        $account->setPassword(password_hash('admin', PASSWORD_DEFAULT));
        $account->setEmail('admin@admin.net');
        $account->setFirstname($faker->firstName());
        $account->setLastname($faker->lastName());
        $account->setBirthdate($faker->dateTimeBetween('-60 years', '-18 years'));
        $account->setSexe($faker->randomElement(['homme', 'femme'])); 
        $account->setIp($faker->ipv4());
        $account->setRoles(['ROLE_ADMIN']);
        $manager->persist($account);

        $manager->flush();
        $manager->clear();
    }
}
