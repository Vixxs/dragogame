# Dragogame

Lien du projet GitLab : [GitLab Dragogame](https://gitlab.com/Vixxs/dragogame)

## Installation

Cloner le projet disponible sur GitLab :
```
git clone https://gitlab.com/Vixxs/dragogame.git
```

Apres avoir cloné le projet, se placer dans le répertoire et lancer le docker-compose :

```
cd dragogame
docker-compose up -d --build
```

Se rendre dans le container 'www_docker_symfony' et installer les dependances composer
```
docker exec -ti www_docker_symfony /bin/bash
composer install
```
Pour ce qui est de la Base de données, l'importer grâce à phpMyAdmin

## Différents liens


### Localhost
```
http://localhost/        Accés au panneau d'administration
http://localhost:8080    Accés à phpMyAdmin
```

### Lien AWS

```
http://ec2-13-38-86-143.eu-west-3.compute.amazonaws.com/        Accés au panneau d'administration
http://ec2-13-38-86-143.eu-west-3.compute.amazonaws.com:8080    Accés à phpMyAdmin

```

## Identifiants

```
utilisateur root pour la bdd :
root
motdepasselong123
```

```
utilisateur admin pour le panneau de configuration :
admin
admin
```

## Logs

Les logs de chaque conteneurs MySQL se trouvent dans les répertoires {master,slave}/logs

## Serveur

Connexion ssh serveur aws avec la clé 'sibd.pem' dans le repertoire où la commande est lancée :
```
ssh -i "sibd.pem" ubuntu@ec2-13-38-86-143.eu-west-3.compute.amazonaws.com
```
